#include "../src/filereader.h"
#include "../src/parsedpacket.h"
#include "../src/protocols.h"
#include "../src/statistics.h"
#include "catch2/catch.hpp"
#include <QString>
#include <QVector>
#include <QtAlgorithms>
#include <algorithm>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

TEST_CASE("Test count_protocols") {
  SECTION("Test count_protocols for empty parsed_packets") {
    // Arrange
    QVector<QString> names = {"IPv4",
                              "IPv6",
                              "TCP",
                              "UDP",
                              "HTTP",
                              "DNS",
                              "FTP",
                              "SSH",
                              "SSL",
                              "ARP",
                              "Generic payload",
                              "Unknown protocol"};
    std::vector<ParsedPacket> parsed_packets = {};
    QVector<int> expeced_output = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    Statistics *s = new Statistics(parsed_packets);
    // Act
    QVector<int> output = s->count_protocols(names);
    delete s;

    // Assert
    REQUIRE(expeced_output == output);
  }
  SECTION("Test count_protocols parsed_packets that has one packet") {
    // Arrange
    QVector<QString> names = {"IPv4",
                              "IPv6",
                              "TCP",
                              "UDP",
                              "HTTP",
                              "DNS",
                              "FTP",
                              "SSH",
                              "SSL",
                              "ARP",
                              "Generic payload",
                              "Unknown protocol"};

    std::string file("testStatistics.pcap");
    pcpp::IFileReaderDevice *reader = pcpp::IFileReaderDevice::getReader(file);
    reader->open();
    pcpp::RawPacket rawPacket;
    if (reader->getNextPacket(rawPacket)) {
    }
    pcpp::Packet packet(&rawPacket);
    ParsedPacket testPacket(packet);
    reader->close();
    std::vector<ParsedPacket> parsed_packets = {testPacket};
    Statistics *s = new Statistics(parsed_packets);
    QVector<int> expeced_output = {1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0};

    // Act
    QVector<int> output = s->count_protocols(names);
    delete s;

    // Assert
    REQUIRE(expeced_output == output);
  }
  SECTION("Test count_protocols parsed_packets that has more packets") {
    // Arrange
    QVector<QString> names = {"IPv4",
                              "IPv6",
                              "TCP",
                              "UDP",
                              "HTTP",
                              "DNS",
                              "FTP",
                              "SSH",
                              "SSL",
                              "ARP",
                              "Generic payload",
                              "Unknown protocol"};

    std::string file("testStatistics.pcap");
    pcpp::IFileReaderDevice *reader = pcpp::IFileReaderDevice::getReader(file);
    reader->open();
    std::vector<ParsedPacket> parsed_packets;
    pcpp::RawPacket rawPacket;
    while (reader->getNextPacket(rawPacket)) {
      pcpp::Packet packet(&rawPacket);
      ParsedPacket testPacket(packet);
      parsed_packets.push_back(testPacket);
    }
    reader->close();
    Statistics *s = new Statistics(parsed_packets);
    QVector<int> expeced_output = {17, 0, 16, 1, 0, 1, 0, 0, 15, 0, 0, 0};

    // Act
    QVector<int> output = s->count_protocols(names);
    delete s;

    // Assert
    REQUIRE(expeced_output == output);
  }
}
TEST_CASE("Test count_sizes") {
  SECTION("Test count_sizes for empty parsed_packets") {
    // Arrange
    QVector<QString> names = {"0-19",    "20-39",   "40-79", "80-159",
                              "160-319", "320-639", "other"};
    std::vector<ParsedPacket> parsed_packets = {};
    QVector<int> expeced_output = {0, 0, 0, 0, 0, 0, 0};
    Statistics *s = new Statistics(parsed_packets);
    std::vector<std::pair<int, int>> intervals = {
        {0, 19},    {20, 39},   {40, 79}, {80, 159},
        {160, 319}, {320, 639}, {-1, -1}};

    // Act
    QVector<int> output = s->count_sizes(names, intervals);
    delete s;

    // assert
    REQUIRE(expeced_output == output);
  }
  SECTION("Test count_sizes for parsed_packets tahat has one packet") {
    // Arrange
    QVector<QString> names = {"0-19",    "20-39",   "40-79", "80-159",
                              "160-319", "320-639", "other"};
    std::string file("testStatistics.pcap");
    pcpp::IFileReaderDevice *reader = pcpp::IFileReaderDevice::getReader(file);
    reader->open();
    pcpp::RawPacket rawPacket;
    if (reader->getNextPacket(rawPacket)) {
    }
    pcpp::Packet packet(&rawPacket);
    ParsedPacket testPacket(packet);
    reader->close();
    std::vector<ParsedPacket> parsed_packets = {testPacket};
    QVector<int> expeced_output = {0, 0, 0, 1, 0, 0, 0};
    Statistics *s = new Statistics(parsed_packets);
    std::vector<std::pair<int, int>> intervals = {
        {0, 19},    {20, 39},   {40, 79}, {80, 159},
        {160, 319}, {320, 639}, {-1, -1}};

    // Act
    QVector<int> output = s->count_sizes(names, intervals);
    delete s;

    // assert
    REQUIRE(expeced_output == output);
  }
  SECTION("Test count_sizes for parsed_packets that has more packets") {
    // Arrange
    QVector<QString> names = {"0-19",    "20-39",   "40-79", "80-159",
                              "160-319", "320-639", "other"};
    std::string file("testStatistics.pcap");
    pcpp::IFileReaderDevice *reader = pcpp::IFileReaderDevice::getReader(file);
    reader->open();
    std::vector<ParsedPacket> parsed_packets;
    pcpp::RawPacket rawPacket;
    while (reader->getNextPacket(rawPacket)) {
      pcpp::Packet packet(&rawPacket);
      ParsedPacket testPacket(packet);
      parsed_packets.push_back(testPacket);
    }
    reader->close();
    QVector<int> expeced_output = {0, 0, 1, 16, 0, 0, 0};
    Statistics *s = new Statistics(parsed_packets);
    std::vector<std::pair<int, int>> intervals = {
        {0, 19},    {20, 39},   {40, 79}, {80, 159},
        {160, 319}, {320, 639}, {-1, -1}};

    // Act
    QVector<int> output = s->count_sizes(names, intervals);
    delete s;

    // assert
    REQUIRE(expeced_output == output);
  }
}
