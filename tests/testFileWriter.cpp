#include "catch2/catch.hpp"

#include "../src/filewriter.h"
#include "pcapplusplus/PcapFileDevice.h"
#include <exception>

TEST_CASE("FileWriter", "[FileWriter]") {
  // pcap library calls PCPP_LOG_ERROR so there will be message in
  // terminal(ignore it).
  SECTION("Constructor throws an std::ios_base::failure if filepath is empty "
          "string or writer doesn't have permission to write to this file ") {
    // Arrange
    std::string filepath("");
    pcpp::LinkLayerType linkLayer = pcpp::LINKTYPE_ETHERNET;

    // Act + Assert
    REQUIRE_THROWS_AS(FileWriter(filepath, linkLayer), std::invalid_argument);
  }

  SECTION("Constructor create object if file is correct pcap file") {
    // Arrange
    std::string file(
        "/home/katarina/Desktop/network-analyzer/tests/createfile.pcap");
    pcpp::LinkLayerType linkLayer = pcpp::LINKTYPE_ETHERNET;

    // Act + Assert
    REQUIRE_NOTHROW(FileWriter(file, linkLayer));
  }
}
