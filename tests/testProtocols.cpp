#include "catch2/catch.hpp"

#include "../src/protocols.h"
#include "pcapplusplus/ArpLayer.h"
#include "pcapplusplus/DnsLayer.h"
#include "pcapplusplus/EthLayer.h"
#include "pcapplusplus/HttpLayer.h"
#include "pcapplusplus/IPv4Layer.h"
#include "pcapplusplus/IPv6Layer.h"
#include "pcapplusplus/PcapFilter.h"
#include "pcapplusplus/ProtocolType.h"
#include "pcapplusplus/TcpLayer.h"
#include "pcapplusplus/UdpLayer.h"
#include "pcapplusplus/VlanLayer.h"
#include <QColor>
#include <iostream>
#include <map>
#include <string>
#include <vector>

TEST_CASE("Test protocol getters", "[class]") {
  // network interface names are hardcoded for my machine
  SECTION("test protocolToString map getter") {
    std::map<pcpp::ProtocolType, std::string> protocolToString = {
        {pcpp::Ethernet, "Eth"},
        {pcpp::EthernetDot3, "EthDot3Layer"},
        {pcpp::MPLS, "MPLS"},
        {pcpp::VLAN, "VLAN"},
        {pcpp::IPv4, "IPv4"},
        {pcpp::IPv6, "IPv6"},
        {pcpp::ARP, "ARP"},
        {pcpp::TCP, "TCP"},
        {pcpp::SSL, "SSL"},
        {pcpp::UDP, "UDP"},
        {pcpp::ICMP, "ICMP"},
        {pcpp::DNS, "DNS"},
        {pcpp::FTP, "FTP"},
        {pcpp::SSH, "SSH"},
        {pcpp::ICMPv6, "ICMPv6"},
        {pcpp::Telnet, "Telnet"},
        {pcpp::PPPoE, "PPoe"},
        {pcpp::GRE, "GRE"},
        {pcpp::PPP_PPTP, "PPP_PPTP"},
        {pcpp::SSL, "SSL"},
        // https://wiki.wireshark.org/SLL.md
        {pcpp::SLL, "SLL"},
        {pcpp::DHCP, "DHCP"},
        {pcpp::NULL_LOOPBACK, "NULL LOOPBACK"},
        {pcpp::IGMP, "IGMP"},
        {pcpp::IGMPv1, "IGMPv1"},
        {pcpp::IGMPv2, "IGMPv2"},
        {pcpp::IGMPv3, "IGMPv3"},
        {pcpp::GenericPayload, "Generic payload"},
        {pcpp::VXLAN, "VXLAN"},
        {pcpp::SIP, "SIP"},
        {pcpp::SDP, "SDP"},
        {pcpp::PacketTrailer, "Packet trailer"},
        {pcpp::Radius, "Radius"},
        {pcpp::GTP, "GTP"},
        {pcpp::BGP, "BGP"}};

    REQUIRE(protocolToString == Protocols::getProtocolToString());
  }

  SECTION("test supported filter protocols") {
    std::vector<pcpp::ProtocolType> supportedFilterProtocols = {
        pcpp::IPv4, pcpp::IPv6, pcpp::ARP, pcpp::TCP, pcpp::UDP};

    REQUIRE(supportedFilterProtocols ==
            Protocols::getSupportedFilterProtocol());
  }

  SECTION("test string to protocol getter") {
    std::map<std::string, pcpp::ProtocolType> strToProtocol = {
        {"Eth", pcpp::Ethernet},
        {"EthDot3Layer", pcpp::EthernetDot3},
        {"MPLS", pcpp::MPLS},
        {"VLAN", pcpp::VLAN},
        {"IPv4", pcpp::IPv4},
        {"IPv6", pcpp::IPv6},
        {"ARP", pcpp::ARP},
        {"TCP", pcpp::TCP},
        {"SSL", pcpp::SSL},
        {"UDP", pcpp::UDP},
        {"ICMP", pcpp::ICMP},
        {"DNS", pcpp::DNS},
        {"FTP", pcpp::FTP},
        {"SSH", pcpp::SSH},
        {"ICMPv6", pcpp::ICMPv6},
        {"Telnet", pcpp::Telnet},
        {"PPoe", pcpp::PPPoE},
        {"GRE", pcpp::GRE},
        {"PPP_PPTP", pcpp::PPP_PPTP},
        {"SSL", pcpp::SSL},
        {"SLL", pcpp::SLL},
        {"DHCP", pcpp::DHCP},
        {"NULL LOOPBACK", pcpp::NULL_LOOPBACK},
        {"IGMP", pcpp::IGMP},
        {"IGMPv1", pcpp::IGMPv1},
        {"IGMPv2", pcpp::IGMPv2},
        {"IGMPv3", pcpp::IGMPv3},
        {"Generic payload", pcpp::GenericPayload},
        {"VXLAN", pcpp::VXLAN},
        {"SIP", pcpp::SIP},
        {"SDP", pcpp::SDP},
        {"Packet trailer", pcpp::PacketTrailer},
        {"Radius", pcpp::Radius},
        {"GTP", pcpp::GTP},
        {"BGP", pcpp::BGP}};

    std::map<std::string, pcpp::ProtocolType> funcCall =
        Protocols::getStringToProtocol();
    REQUIRE(strToProtocol == Protocols::getStringToProtocol());
  }
}

TEST_CASE("Test toString method", "[class]") {
  SECTION("test for existing protocol") {
    pcpp::ProtocolType proto = pcpp::ARP;
    REQUIRE(Protocols::toString(proto) == "ARP");
  }

  SECTION("test for random number") {
    pcpp::ProtocolType proto = 2353464;
    REQUIRE(Protocols::toString(proto) == "Unknown protocol");
  }

  SECTION("test for negative number") {
    pcpp::ProtocolType proto = -32;
    REQUIRE(Protocols::toString(proto) == "Unknown protocol");
  }
}

TEST_CASE("Testing QColor getter", "[class]") {
  SECTION("test default value") {
    pcpp::ProtocolType proto = pcpp::TCP;
    QColor color(194, 134, 219, 255);
    REQUIRE(Protocols::getColor(proto) == color);
  }

  SECTION("test protocol with undefined color", "[class]") {
    pcpp::ProtocolType proto = pcpp::GRE;
    REQUIRE(Protocols::getColor(proto) == Qt::transparent);
  }
}
