#include "catch2/catch.hpp"

#include "../src/filereader.h"
#include "../src/parsedpacket.h"
#include "../src/sessiondata.h"
#include <string>
#include <vector>
TEST_CASE("Testing does parsedPacket parse src and destination correctly") {
  SECTION("Packet has ipv4 protocol, getter for src and dest returns correct "
          "address") {
    // Arrange
    std::string expectedSrc("10.0.0.138");
    std::string expectedDest("10.0.0.1");

    std::string file("testIPV4.pcap");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    reader.readPcapFile();
    ParsedPacket testPacket = sessionData->getParsedPackets()[0];

    // Act
    std::string testSrc = testPacket.getSrcAdress();
    std::string testDest = testPacket.getDestAdress();

    // Assert
    REQUIRE(testSrc == expectedSrc);
    REQUIRE(testDest == expectedDest);
  }
  SECTION("Packet has ipv6 protocol, getter for src and dest returns correct "
          "address") {
    // Arrange
    std::string expectedSrc("fe80::41:d5ff:fee9:322d");
    std::string expectedDest("ff02::1");

    std::string file("testIP6.pcap");

    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    reader.readPcapFile();
    ParsedPacket testPacket = sessionData->getParsedPackets()[0];

    // Act
    std::string testSrc = testPacket.getSrcAdress();
    std::string testDest = testPacket.getDestAdress();

    // Assert
    REQUIRE(testSrc == expectedSrc);
    REQUIRE(testDest == expectedDest);
  }
  SECTION("Packet has arp protocol, getter for src and dest returns correct "
          "address") {
    // Arrange
    std::string expectedSrc("192.168.0.1");
    std::string expectedDest("192.168.0.204");

    std::string file("testJustARP.pcap");

    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    reader.readPcapFile();
    ParsedPacket testPacket = sessionData->getParsedPackets()[0];

    // Act
    std::string testSrc = testPacket.getSrcAdress();
    std::string testDest = testPacket.getDestAdress();

    // Assert
    REQUIRE(testSrc == expectedSrc);
    REQUIRE(testDest == expectedDest);
  }
  SECTION("Packet doesn't have any from this above  protocols  or is empty, "
          "getter returns default string  ") {
    // Arrange
    std::string expectedSrc("-");
    std::string expectedDest("-");
    pcpp::RawPacket rawPacket;
    pcpp::Packet packet(&rawPacket);
    ParsedPacket testPacket(packet);

    // Act
    std::string testSrc = testPacket.getSrcAdress();
    std::string testDest = testPacket.getDestAdress();

    // Assert
    REQUIRE(testSrc == expectedSrc);
    REQUIRE(testDest == expectedDest);
  }
}

TEST_CASE("Testing does getter for size of packet returns correct size",
          "[function]") {
  SECTION("Packet is not empty, getter returns correct size in bytes") {
    // Arrange
    int expectedSize = 407;
    std::string file("testIPV4.pcap");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    reader.readPcapFile();
    ParsedPacket testPacket = sessionData->getParsedPackets()[0];

    // Act
    int testSize = testPacket.getSize();

    // Assert
    REQUIRE(testSize == expectedSize);
  }
  SECTION("Packet is empty, getter returns 0") {
    // Arrange
    int expectedSize = 0;
    pcpp::RawPacket rawPacket;
    pcpp::Packet packet(&rawPacket);
    ParsedPacket testPacket(packet);

    // Act
    int testSize = testPacket.getSize();

    // Assert
    REQUIRE(testSize == expectedSize);
  }
}

TEST_CASE("Testing does getter for rawBytes returns correct string") {
  SECTION("Packet returns correct string of rawbytes") {
    // Arrange
    std::string expectedRawBytes("\n\
0:14 13 33 74 57 e1 b0  a d5 82 bc 4e  8  6  0  1 \n\
1: 8  0  6  4  0  1 b0  a d5 82 bc 4e c0 a8  0  1 \n\
2: 0  0  0  0  0  0 c0 a8  0 cc ");

    std::string file("testJustARP.pcap");

    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    reader.readPcapFile();
    ParsedPacket testPacket = sessionData->getParsedPackets()[0];

    // Act
    std::string testRawBytes(testPacket.getPacketBytes());

    // Assert
    REQUIRE(testRawBytes == expectedRawBytes);
  }
  SECTION("Packet is empty, getter returns empty string") {
    // Arrange
    std::string expectedAsciiBytes("");
    pcpp::RawPacket rawPacket;
    pcpp::Packet packet(&rawPacket);
    ParsedPacket testPacket(packet);

    // Act
    std::string testAsciiBytes(testPacket.getPacketBytes());

    // Assert
    REQUIRE(testAsciiBytes == expectedAsciiBytes);
  }
}

TEST_CASE("Testing does getter for ascii packet bytes returns correct string") {
  SECTION("Packet returns correct ascii string when packet is not empty") {
    std::string expectedAsciiBytes("\n\
0:..3tW......N....\n\
1:...........N....\n\
2:..........");

    std::string file("testJustARP.pcap");

    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    reader.readPcapFile();
    ParsedPacket testPacket = sessionData->getParsedPackets()[0];

    // Act
    std::string testAsciiBytes(testPacket.getPacketBytesAscii());

    // Assert
    REQUIRE(testAsciiBytes == expectedAsciiBytes);
  }
  SECTION("Packet returns empty string when packet is empty") {
    // Arrange
    std::string expectedAsciiBytes("");
    pcpp::RawPacket rawPacket;
    pcpp::Packet packet(&rawPacket);
    ParsedPacket testPacket(packet);

    // Act
    std::string testAsciiBytes(testPacket.getPacketBytesAscii());

    // Assert
    REQUIRE(testAsciiBytes == expectedAsciiBytes);
  }
}
