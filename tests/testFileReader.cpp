#include "catch2/catch.hpp"

#include "../src/filereader.h"
#include "../src/sessiondata.h"
#include <exception>
#include <pcapplusplus/PcapFilter.h>

#include <exception>
TEST_CASE("FileReader", "[FileReader]") {
  // pcap library calls PCPP_LOG_ERROR so there will be message in
  // terminal(ignore it).
  SECTION("Constructor throws std::ios_base::failure if filepath is incorrect "
          "pcap file ") {
    // Arrange
    std::string file("incorrect.pcap");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    // Act + Assert
    REQUIRE_THROWS_AS(FileReader(file, sessionData), std::ios_base::failure);
  }
  SECTION("Constructor throws std::ios_base::failure if file doesn't exists") {
    // Arrange
    std::string file("");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    // Act + Assert
    REQUIRE_THROWS_AS(FileReader(file, sessionData), std::ios_base::failure);
  }
  SECTION("Constructor creates object if filepath is correct pcap file") {
    // Arrange
    std::string file("correctfile.pcap");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();
    // Act + Assert
    REQUIRE_NOTHROW(FileReader(file, sessionData));
  }

  SECTION("setFilter throws exception std::invalid_argument if filters are "
          "incorrect") {
    // Arrange
    std::string file("correctfile.pcap");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    std::vector<pcpp::GeneralFilter *> filters;
    // incorrect format for srcip address
    std::string badSrcIp("incorrectformat");
    pcpp::IPFilter ipFilter = pcpp::IPFilter(badSrcIp, pcpp::SRC);
    filters.push_back(&ipFilter);

    // Act + Assert
    REQUIRE_THROWS_AS(reader.set_filter(filters), std::invalid_argument);
  }
  // this examples where tested in wireshark
  SECTION("All packets in file have selected protocoltype, all of them pass "
          "protocoltype filter") {
    // Arrange
    // file with only arp packets
    std::string file("testJustARP.pcap");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    std::vector<pcpp::GeneralFilter *> filters;

    // select protocoltype
    pcpp::ProtoFilter protocolFilter = pcpp::ProtoFilter(pcpp::ARP);

    filters.push_back(&protocolFilter);
    // act
    reader.set_filter(filters);
    reader.readPcapFile();
    std::vector<ParsedPacket> packets = sessionData->getParsedPackets();

    // assert
    REQUIRE(packets.size() == 2);
  }
  SECTION("Only packets with selected protocoltype pass filter") {
    // Arrange
    // file with many different packets
    std::string file("testfilters.pcap");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    std::vector<pcpp::GeneralFilter *> filters;

    pcpp::ProtoFilter protocolFilter = pcpp::ProtoFilter(pcpp::IPv4);

    filters.push_back(&protocolFilter);
    // act
    reader.set_filter(filters);
    reader.readPcapFile();
    std::vector<ParsedPacket> packets = sessionData->getParsedPackets();

    // assert
    REQUIRE(packets.size() == 6);
  }
  SECTION("Only packets with selected src ip and ip port  pass filter") {
    // Arrange

    // same file from last section with differenet filters.
    std::string file("testfilters.pcap");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();

    FileReader reader = FileReader(file, sessionData);
    std::vector<pcpp::GeneralFilter *> filters;

    pcpp::ProtoFilter protocolFilter = pcpp::ProtoFilter(pcpp::IPv4);
    filters.push_back(&protocolFilter);
    std::string srcIp("10.81.0.245");
    pcpp::IPFilter ipFilter = pcpp::IPFilter(srcIp, pcpp::SRC);
    filters.push_back(&ipFilter);
    pcpp::PortFilter portFIlter = pcpp::PortFilter(443, pcpp::Direction::DST);
    filters.push_back(&portFIlter);

    // act
    reader.set_filter(filters);
    reader.readPcapFile();
    std::vector<ParsedPacket> packets = sessionData->getParsedPackets();

    // assert
    REQUIRE(packets.size() == 3);
  }
  SECTION("No packets pass filter with selected src ip and port") {

    // same file from last example with different port
    std::string file("testfilters.pcap");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();
    FileReader reader = FileReader(file, sessionData);
    std::vector<pcpp::GeneralFilter *> filters;

    pcpp::ProtoFilter protocolFilter = pcpp::ProtoFilter(pcpp::TCP);
    filters.push_back(&protocolFilter);
    std::string srcIp("10.81.0.245");
    pcpp::IPFilter ipFilter = pcpp::IPFilter(srcIp, pcpp::SRC);
    filters.push_back(&ipFilter);
    pcpp::PortFilter portFIlter = pcpp::PortFilter(47598, pcpp::Direction::DST);
    filters.push_back(&portFIlter);

    // act
    reader.set_filter(filters);
    reader.readPcapFile();
    std::vector<ParsedPacket> packets = sessionData->getParsedPackets();
    // assert
    REQUIRE(packets.size() == 0);
  }
}
