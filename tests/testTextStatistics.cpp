#include "../src/TextStatistics.h"
#include "../src/filereader.h"
#include "../src/parsedpacket.h"
#include "../src/protocols.h"
#include "catch2/catch.hpp"
#include <QString>
#include <QVector>
#include <QtAlgorithms>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

TEST_CASE("Test function numElementsInRange", "[TextStatistics]") {
  SECTION("Test numElementsInRange function for vector with few elements") {
    int begin = 10;
    int end = 20;
    QVector<int> inputVector{14, 19, 17, 3, 9, 111, 68, 52, 34};
    double expectedOutput = 3;

    TextStatistics textStatistics = TextStatistics();
    double output = textStatistics.numElementsInRange(begin, end, inputVector);
    REQUIRE(expectedOutput == output);
  }

  SECTION("Test function numElementsInRange for vector with one element") {
    int begin = 11;
    int end = 14;
    QVector<int> inputVector{1};
    double expectedOutput = 0;

    TextStatistics textStatistics = TextStatistics();
    double output = textStatistics.numElementsInRange(begin, end, inputVector);
    REQUIRE(expectedOutput == output);
  }

  SECTION("Test function numElementsInRange for empty vector") {
    int begin = 11;
    int end = 14;
    QVector<int> inputVector;
    double expectedOutput = 0;

    TextStatistics textStatistics = TextStatistics();
    double output = textStatistics.numElementsInRange(begin, end, inputVector);
    REQUIRE(expectedOutput == output);
  }
}

TEST_CASE("Test getSrcDestMap function", "[TextStatistics]") {
  SECTION("Test with empty vector") {
    std::vector<ParsedPacket> emptyParsedPackets;

    TextStatistics textStatistics = TextStatistics();
    auto result = textStatistics.getSrcDestMap(emptyParsedPackets);
    REQUIRE(result.empty());
  }
  SECTION("Test with parsed packet that has one file") {
    std::string file("/home/marijana/Fax/rs/network-analyzer/tests/"
                     "fiesForTests/testStatistics.pcap");
    pcpp::IFileReaderDevice *reader = pcpp::IFileReaderDevice::getReader(file);
    reader->open();
    pcpp::RawPacket rawPacket;
    if (reader->getNextPacket(rawPacket)) {
    }
    pcpp::Packet packet(&rawPacket);
    ParsedPacket testPacket(packet);
    reader->close();
    std::vector<ParsedPacket> parsed_packets = {testPacket};

    TextStatistics textStatistics = TextStatistics();
    auto result = textStatistics.getSrcDestMap(parsed_packets);
    std::string src = "192.168.10.105";
    std::string dest = "224.0.0.251";
    int count = 1;
    std::pair<std::string, std::string> expected_key = {"192.168.10.105",
                                                        "224.0.0.251"};

    REQUIRE(count == result[expected_key]);
  }
}
