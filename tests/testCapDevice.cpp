#include "catch2/catch.hpp"

#include "../src/capdevice.h"
#include "../src/sessiondata.h"
#include "pcapplusplus/ArpLayer.h"
#include "pcapplusplus/DnsLayer.h"
#include "pcapplusplus/EthLayer.h"
#include "pcapplusplus/HttpLayer.h"
#include "pcapplusplus/IPv4Layer.h"
#include "pcapplusplus/IPv6Layer.h"
#include "pcapplusplus/PcapFilter.h"
#include "pcapplusplus/ProtocolType.h"
#include "pcapplusplus/TcpLayer.h"
#include "pcapplusplus/UdpLayer.h"
#include "pcapplusplus/VlanLayer.h"
#include <string>
#include <vector>

TEST_CASE("Testing capture device opening", "[class]") {
  // network interface names are hardcoded for my machine
  SECTION("Network device opens for following network interfaces") {
    std::vector<std::string> interfaceNames{"wlp1s0", "any", "lo", "enp2s0"};

    CHECK_NOTHROW(CapDevice(interfaceNames[0], nullptr));
    CHECK_NOTHROW(CapDevice(interfaceNames[1], nullptr));
    CHECK_NOTHROW(CapDevice(interfaceNames[2], nullptr));
    CHECK_NOTHROW(CapDevice(interfaceNames[3], nullptr));
  }

  SECTION("Testing if for nfqueue interface device cannot be opened") {
    std::string interfaceName("nfqueue");
    // nemam pojma kako da napisem da proverava da li mi se hvata izuzetak
    // ako se taj isti izuzetak hvata u samom konstruktoru
    CHECK_THROWS_AS(CapDevice(interfaceName, nullptr), std::runtime_error);
    // }
  }

  SECTION("Test empty constructor") {
    CHECK_THROWS_AS(CapDevice(), std::invalid_argument);
  }

  SECTION("Testing invalid name") {
    std::string invalidInterfaceName("abcdefgh");
    CHECK_THROWS_AS(CapDevice(invalidInterfaceName, nullptr),
                    std::invalid_argument);
  }
}

TEST_CASE("Testing adding filters", "[class]") {
  SECTION("Testing if adding all protocol filters throws exception "
          "(expression rejects all packets)") {
    pcpp::ProtoFilter *ipv4 = new pcpp::ProtoFilter(pcpp::IPv4);
    pcpp::ProtoFilter *ipv6 = new pcpp::ProtoFilter(pcpp::IPv6);
    pcpp::ProtoFilter *arp = new pcpp::ProtoFilter(pcpp::ARP);
    pcpp::ProtoFilter *tcp = new pcpp::ProtoFilter(pcpp::TCP);
    pcpp::ProtoFilter *udp = new pcpp::ProtoFilter(pcpp::UDP);

    std::vector<pcpp::GeneralFilter *> protocolFilters = {ipv4, ipv6, arp, tcp,
                                                          udp};
    std::string interfaceName("any");
    CapDevice device(interfaceName);

    CHECK_THROWS_AS(device.set_filter(protocolFilters), std::invalid_argument);

    for (auto protoFilter : protocolFilters) {
      delete protoFilter;
    }
  }

  SECTION("Test ip filters") {
    pcpp::IPFilter *src = new pcpp::IPFilter("195.90.145.240", pcpp::SRC);
    // private ip address
    pcpp::IPFilter *dst = new pcpp::IPFilter("172.16.0.0", pcpp::DST);

    std::vector<pcpp::GeneralFilter *> ipFilters = {src, dst};
    std::string interfaceName("any");

    CapDevice device(interfaceName);

    CHECK_NOTHROW(device.set_filter(ipFilters));

    for (auto ipFilter : ipFilters) {
      delete ipFilter;
    }
  }

  SECTION("Test port filters") {
    unsigned srcPort = 40;
    unsigned dstPort = 60;
    pcpp::PortFilter *src = new pcpp::PortFilter(srcPort, pcpp::SRC);
    // private ip address
    pcpp::PortFilter *dst = new pcpp::PortFilter(dstPort, pcpp::DST);

    std::vector<pcpp::GeneralFilter *> portFilters = {src, dst};
    std::string interfaceName("any");

    CapDevice device(interfaceName);

    CHECK_NOTHROW(device.set_filter(portFilters));

    for (auto portFilter : portFilters) {
      delete portFilter;
    }
    // add for empty vector and add for cap time also
  }
}

TEST_CASE("Testing time accuracy in capture session data", "[class]") {
  SECTION("Testing in start capture time is accurate") {
    std::string interfaceName("any");

    CapDevice device(interfaceName);
    long startTimeSec, startTimeNanosec;
    long stopTimeSec, stopTimeNanosec;
    long sleepTime = 10;

    pcpp::clockGetTime(startTimeSec, startTimeNanosec);
    device.start_capture();
    // sleep for sleepTime seconds in main thread, in the meantime packets
    // are captured in the async thread
    pcpp::multiPlatformSleep(sleepTime);
    device.stop_capture();
    pcpp::clockGetTime(stopTimeSec, stopTimeNanosec);

    REQUIRE(startTimeSec == device.startCaptureTime());
    REQUIRE(stopTimeSec == device.endCaptureTime());

    long capDuration = stopTimeSec - startTimeSec;
    REQUIRE(capDuration == device.captureSessionDuration());
  }

  SECTION("Testing in start capture time is accurate when i have packet data "
          "created") {
    std::string interfaceName("any");
    std::shared_ptr<SessionData> sessionData = std::make_shared<SessionData>();
    CapDevice device(interfaceName, sessionData);
    long startTimeSec, startTimeNanosec;
    long stopTimeSec, stopTimeNanosec;
    long sleepTime = 10;

    pcpp::clockGetTime(startTimeSec, startTimeNanosec);
    device.start_capture();
    // sleep for sleepTime seconds in main thread, in the meantime packets
    // are captured in the async thread
    pcpp::multiPlatformSleep(sleepTime);
    device.stop_capture();
    pcpp::clockGetTime(stopTimeSec, stopTimeNanosec);
    //+1 second for latency
    REQUIRE(startTimeSec == device.startCaptureTime());
    REQUIRE(stopTimeSec == device.endCaptureTime());

    long capDuration = stopTimeSec - startTimeSec;
    REQUIRE(capDuration == device.captureSessionDuration());
  }
}
