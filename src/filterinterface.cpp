#include "filterinterface.h"
#include "protocols.h"
#include "ui_filterinterface.h"
#include <QDebug>

// Podrzani filteri (man pcap-filter) : ether, fddi, tr, wlan, ip, ip6, arp,
// rarp, decnet, sctp, tcp and udp. (wrapper ne podrzava sve ove)
FilterInterface::FilterInterface(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::FilterInterface) {
  ui->setupUi(this);
  int i = 0;//NOLINT

  std::vector<pcpp::ProtocolType> supportedProtocols =
      Protocols::getSupportedFilterProtocol();
  for (auto protocol : supportedProtocols) {
    auto item = new QListWidgetItem(
        QString::fromStdString(Protocols::toString(protocol)), ui->listWidget);
    item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
    item->setCheckState(Qt::Unchecked);
    i++;
  }
  ui->listWidget->sortItems();
  m_portDst = false;
  m_portSrc = false;
  m_srcIp = "";
  m_dstIp = "";
  m_port = -1;
  m_filters = std::vector<pcpp::GeneralFilter *>();
}

FilterInterface::~FilterInterface() {
  for (auto & m_filter : m_filters) {
    delete m_filter;
  }
  delete ui;
}

void
    FilterInterface::IPSrcCheck_stateChanged(int arg1) {
  m_srcIp = ui->srcIP->displayText().toStdString();
}

void
    FilterInterface::IPDestCheck_stateChanged(int arg1) {
  if (arg1 != 0)//NOLINT
    ui->destIP->displayText();
  m_dstIp = ui->destIP->displayText().toStdString();
}

void
    FilterInterface::srcPortCheck_clicked() {
  m_portSrc = ~m_portSrc;
}

void
    FilterInterface::destPortCheck_clicked() {
  m_portDst = ~m_portDst;
}

void
    FilterInterface::submitFilters_accepted() {
  if (!m_srcIp.empty()) {
    auto *ipFilter = new pcpp::IPFilter(m_srcIp, pcpp::SRC);
    m_filters.push_back(ipFilter);
  }
  if (!m_dstIp.empty()) {
    auto *ipFilter = new pcpp::IPFilter(m_dstIp, pcpp::DST);
    m_filters.push_back(ipFilter);
  }

  auto stringToProtocol = Protocols::getStringToProtocol();
  for (int i = 0; i < ui->listWidget->count(); i++) {
    auto* item = ui->listWidget->item(i);
    if (item->checkState() == Qt::CheckState::Checked) {
      std::string protoName = item->text().toStdString();
      pcpp::ProtocolType proto = stringToProtocol[protoName];
      auto *protoFilter = new pcpp::ProtoFilter(proto);
      m_filters.push_back(protoFilter);
    }
  }

  if (m_portSrc) {
    auto *portFIlter =
        new pcpp::PortFilter(m_port, pcpp::Direction::SRC);
    m_filters.push_back(portFIlter);
  }
  if (m_portDst) {
    auto *portFIlter =
        new pcpp::PortFilter(m_port, pcpp::Direction::DST);
    m_filters.push_back(portFIlter);
  }
}

std::vector<pcpp::GeneralFilter *> &
    FilterInterface::filters() {
  return m_filters;
}

void
    FilterInterface::spinBox_editingFinished() {
  m_port = ui->spinBox->text().toInt();
}
