#ifndef FILEREADER_H
#define FILEREADER_H

#include "parsedpacket.h"
#include "pcapplusplus/PcapFileDevice.h"
#include "pcapplusplus/PcapLiveDeviceList.h"
#include "pcapplusplus/SystemUtils.h"
#include "sessiondata.h"
#include "stdlib.h"
#include <exception>
#include <iostream>
#include <string>
#include <typeinfo>

#include <QObject>

class FileReader : public QObject {
    Q_OBJECT
  public:
    FileReader(std::string &path, std::shared_ptr<SessionData> sessionData);
    ~FileReader();
    void
        readPcapFile();
    std::string
        getFilePath();
    void
        set_filter(std::vector<pcpp::GeneralFilter *> &filter);

  signals:
    void
        packetRead(pcpp::RawPacket rawPacket);

  private:
    std::string m_path;
    pcpp::IFileReaderDevice *m_reader;
    std::shared_ptr<SessionData> m_sessionData;
};

#endif // FILEREADER_H
