#include "sessiondata.h"

// zasto uopste imamo ovaj bool kad svakako necemo kreirati instancu ove klase
// ako se ne pise u fajl?
SessionData::SessionData() = default;

// destruction of raw packets is done automatically by pcapplusplus library
SessionData::~SessionData() {}

void
    SessionData::push(pcpp::RawPacket *rawPacket) {

  emit(writePacketToFile(rawPacket));//NOLINT
  pcpp::Packet packet(rawPacket);
  ParsedPacket parsedPacket(packet);
  m_parsedPacketVector.push_back(parsedPacket);

  emit(packetPushed(parsedPacket));//NOLINT
}

void
    SessionData::savePacketCount(uint64_t packetsRevc,
                                 uint64_t packetsDropped) {
  m_packetCount.push_back(std::make_pair(packetsRevc, packetsDropped));
}

void
    SessionData::saveCaptureTime(long start, long end) {
  m_captureTimes.push_back(std::make_pair(start, end));
}

std::vector<ParsedPacket> &
    SessionData::getParsedPackets() {
  return m_parsedPacketVector;
}

std::vector<std::pair<long, long>> &
    SessionData::getCaptureTimes() {
  return m_captureTimes;
}

std::vector<std::pair<uint64_t, uint64_t>> &
    SessionData::getPacketCount() {
  return m_packetCount;
}
