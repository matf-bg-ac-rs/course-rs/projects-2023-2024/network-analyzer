#include "parsedpacket.h"
#include "pcapplusplus/PcapFilter.h"
#include "sessiondata.h"
#include <iostream>
#include <pcapplusplus/EthDot3Layer.h>
#include <pcapplusplus/IcmpLayer.h>
#include <pcapplusplus/IcmpV6Layer.h>
#include <pcapplusplus/IgmpLayer.h>
#include <pcapplusplus/PacketTrailerLayer.h>
#include <pcapplusplus/PayloadLayer.h>
#include <pcapplusplus/SSLLayer.h>

ParsedPacket::ParsedPacket(pcpp::Packet &packet)
    : m_size(packet.getRawPacket()->getRawDataLen())
    , m_ipAddr(parseSrcAndDest(packet))
    , m_bytesToAscii(getPacketBytesAscii(packet.getRawPacket()))
    , m_rawBytes(getPacketBytes(packet.getRawPacket()))
    , m_srcDestPorts(parsePortDestSrc(packet)) {

  m_packetCol = Qt::transparent;
  for (pcpp::Layer *curLayer = packet.getFirstLayer(); curLayer != nullptr;
       curLayer = curLayer->getNextLayer()) {
    auto proto = curLayer->getProtocol();
    m_protocol.push_back(proto);
    if (Protocols::getColor(proto) != Qt::transparent)//NOLINT
      m_packetCol = Protocols::getColor(proto);
  }

  initializeLayerInfo(packet);
}

void
    ParsedPacket::initializeLayerInfo(pcpp::Packet &packet) {
  // the option was for this to be in a map but we can't cause pcpp::Layer is
  // abstract class
  std::string m_eth = parseLayer<pcpp::EthLayer>(packet, pcpp::Ethernet);
  std::string m_ipv4 = parseLayer<pcpp::IPv4Layer>(packet, pcpp::IPv4);
  std::string m_ipv6 = parseLayer<pcpp::IPv6Layer>(packet, pcpp::IPv6);
  std::string m_tcp = parseLayer<pcpp::TcpLayer>(packet, pcpp::TCP);
  std::string m_udp = parseLayer<pcpp::UdpLayer>(packet, pcpp::UDP);
  std::string m_ssl = parseLayer<pcpp::SSLLayer>(packet, pcpp::SSL);
  std::string m_http_req =
      parseLayer<pcpp::HttpRequestLayer>(packet, pcpp::HTTPRequest);
  std::string m_http_res =
      parseLayer<pcpp::HttpResponseLayer>(packet, pcpp::HTTPResponse);
  std::string m_arp = parseLayer<pcpp::ArpLayer>(packet, pcpp::ARP);
  std::string m_dns = parseLayer<pcpp::DnsLayer>(packet, pcpp::DNS);
  std::string m_icmp = parseLayer<pcpp::IcmpLayer>(packet, pcpp::ICMP);
  std::string m_igmp = parseLayer<pcpp::IgmpLayer>(packet, pcpp::IGMP);
  std::string m_packetTrailer =
      parseLayer<pcpp::PacketTrailerLayer>(packet, pcpp::PacketTrailer);
  std::string m_genericPayload =
      parseLayer<pcpp::PayloadLayer>(packet, pcpp::GenericPayload);
  m_layerInfo = {m_eth,  m_ipv4,     m_ipv6,          m_tcp,           m_udp,
                 m_ssl,  m_http_req, m_http_res,      m_arp,           m_dns,
                 m_icmp, m_igmp,     m_packetTrailer, m_genericPayload};
}

std::vector<std::string> &
    ParsedPacket::getLayerInfo() {
  return m_layerInfo;
}

int
    ParsedPacket::getSize() const {
  return m_size;
}
std::string &
    ParsedPacket::getSrcAdress() {
  return m_ipAddr.first;
}

std::string &
    ParsedPacket::getDestAdress() {
  return m_ipAddr.second;
}

std::pair<uint16_t, uint16_t>
    ParsedPacket::getPortSrcAndDest() {
  return m_srcDestPorts;
}

std::vector<pcpp::ProtocolType> &
    ParsedPacket::getProtocols() {
  return m_protocol;
}

std::string &
    ParsedPacket::getPacketBytes()  {
  return m_rawBytes;
}
std::string &
    ParsedPacket::getPacketBytesAscii()  {
  return m_bytesToAscii;
}

QColor &
    ParsedPacket::getPacketCol() {
  return m_packetCol;
}

std::string
    ParsedPacket::outputForGUI() {
  std::string protocols = protocolsToString();
  std::string ports =
      (m_srcDestPorts.first != 0 && m_srcDestPorts.second != 0)
          ? "Ports \t SRC: " + std::to_string(m_srcDestPorts.first) +
                "\tDST: " + std::to_string(m_srcDestPorts.second)
          : "";
  return std::to_string(m_size) + "[B]\t" + m_ipAddr.first + "\t->\t" +
         m_ipAddr.second + "\t" + protocols + "\t" + ports;
}

std::string
    ParsedPacket::protocolsToString() {
  std::string protocolString;
  for (auto protocol : m_protocol){
    protocolString.append(Protocols::toString(protocol) + " | ");
  }
  return protocolString;
}
std::pair<uint16_t, uint16_t>
    ParsedPacket::parsePortDestSrc(const pcpp::Packet &packet) {//NOLINT

  uint16_t src = 0;
  uint16_t dest = 0;

  if (packet.isPacketOfType(pcpp::TCP)) {
    auto* tcp = packet.getLayerOfType<pcpp::TcpLayer>();
    src = tcp->getSrcPort();
    dest = tcp->getDstPort();
    // std::cout << tcp->getTcpOption();
  } else if (packet.isPacketOfType(pcpp::UDP)) {
    auto* udp = packet.getLayerOfType<pcpp::UdpLayer>();
    src = udp->getSrcPort();
    dest = udp->getDstPort();
  }
  return std::make_pair(src, dest);
}

std::pair<std::string, std::string>
    ParsedPacket::parseSrcAndDest(const pcpp::Packet &packet) {//NOLINT
  std::string src;
  std::string dest;

  if (packet.isPacketOfType(pcpp::IPv4)) {
    auto *ipLayer = packet.getLayerOfType<pcpp::IPv4Layer>();

    src = ipLayer->getSrcIPAddress().toString();
    dest = ipLayer->getDstIPAddress().toString();

  } else if (packet.isPacketOfType(pcpp::IPv6)) {
    auto *ipLayer = packet.getLayerOfType<pcpp::IPv6Layer>();

    src = ipLayer->getSrcIPAddress().toString();
    dest = ipLayer->getDstIPAddress().toString();
  } else if (packet.isPacketOfType(pcpp::ARP)) {
    auto *ipLayer = packet.getLayerOfType<pcpp::ArpLayer>();

    src = ipLayer->getSenderIpAddr().toString();
    dest = ipLayer->getTargetIpAddr().toString();
  } else {
    src = "-";
    dest = "-";
  }

  return std::make_pair(src, dest);
}

// https://www.wireshark.org/docs/wsug_html_chunked/ChUsePacketBytesPaneSection.html
std::string
    ParsedPacket::getPacketBytes(pcpp::RawPacket *rawPacket) const{
  std::stringstream s;
  const unsigned char *rawData = rawPacket->getRawData();
  for (int i = 0; i < m_size; i++) {
    if (i % 16 == 0) {
      s << std::endl;
      int hnum = i / 16;
      s << std::hex << hnum << ":";
    }
    s << std::setw(2) << std::hex << static_cast<unsigned>(rawData[i]) << " ";
  }
  return s.str();
}

// optimize?+
std::string
    ParsedPacket::getPacketBytesAscii(pcpp::RawPacket *rawPacket) const {
  std::stringstream s;
  const unsigned char *rawData = rawPacket->getRawData();

  for (unsigned i = 0; i < m_size; i++) {
    if (i % 16 == 0) {
      s << std::endl;
      int hnum = i / 16;
      s << std::hex << hnum << ":";
    }
    s << (isprint(rawData[i]) ? static_cast<char>(rawData[i]) : '.');
  }
  return s.str();
}
