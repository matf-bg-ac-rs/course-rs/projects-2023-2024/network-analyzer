#include "TextStatistics.h"
#include "parsedpacket.h"
#include "sessiondata.h"
#include <QAction>
#include <QFileDialog>
#include <QMenuBar>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QVector>
#include <QWidget>
#include <QtCharts/QChartView>
#include <QtWidgets>
#include <algorithm>
#include <time.h>

TextStatistics::TextStatistics(QWidget *parent)
    : QWidget{parent} {}

double
    TextStatistics::numElementsInRange(int a, int b, QVector<int> &v) {
  // QVector<int> v = {1,10,25,11};
  double counter = std::count_if(v.begin(), v.end(),
                                 [a, b](int x) { return x >= a && x <= b; });
  return counter;
}

std::map<std::pair<std::string, std::string>, int>
    TextStatistics::getSrcDestMap(std::vector<ParsedPacket> parsedPacket) {
  std::map<std::pair<std::string, std::string>, int> count;
  for (auto &x : parsedPacket) {
    std::pair<std::string, std::string> srcDestPair =
        std::make_pair(x.getSrcAdress(), x.getDestAdress());
    count[srcDestPair]++;
  }
  return count;
}

void
    TextStatistics::size(std::vector<ParsedPacket> parsedPacket) {
  QWidget *widget = new QWidget();
  QVBoxLayout *textLayout = new QVBoxLayout(widget);

  QTextEdit *textEdit = new QTextEdit(widget);
  textEdit->setReadOnly(true);

  QVector<int> sizes;
  sizes.reserve(1000);
  for (auto &x : parsedPacket) {
    sizes.push_back(x.getSize());
  }

  textEdit->insertPlainText("Packet lengths:  "
                            "0-19\t20-39\t40-79\t80-159\t160-319\t320-"
                            "639\tother\nCount:\t         ");
  textEdit->insertPlainText(
      QString::number(TextStatistics::numElementsInRange(0, 19, sizes)));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(
      QString::number(TextStatistics::numElementsInRange(20, 39, sizes)));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(
      QString::number(TextStatistics::numElementsInRange(40, 79, sizes)));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(
      QString::number(TextStatistics::numElementsInRange(80, 159, sizes)));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(
      QString::number(TextStatistics::numElementsInRange(160, 319, sizes)));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(
      QString::number(TextStatistics::numElementsInRange(320, 639, sizes)));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(
      QString::number(TextStatistics::numElementsInRange(640, 10000, sizes)));
  textEdit->insertPlainText("\n");

  // if n is 0 you have Nan value, see if you want that
  double n = parsedPacket.size();
  textEdit->insertPlainText("Percentage:      ");
  textEdit->insertPlainText(QString::number(
      TextStatistics::numElementsInRange(0, 19, sizes) / n, 'f', 2));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(QString::number(
      TextStatistics::numElementsInRange(20, 39, sizes) / n, 'f', 2));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(QString::number(
      TextStatistics::numElementsInRange(40, 79, sizes) / n, 'f', 2));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(QString::number(
      TextStatistics::numElementsInRange(80, 159, sizes) / n, 'f', 2));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(QString::number(
      TextStatistics::numElementsInRange(160, 319, sizes) / n, 'f', 2));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(QString::number(
      TextStatistics::numElementsInRange(320, 639, sizes) / n, 'f', 2));
  textEdit->insertPlainText("\t");
  textEdit->insertPlainText(QString::number(
      TextStatistics::numElementsInRange(640, 10000, sizes) / n, 'f', 2));
  textEdit->insertPlainText("\n");
  textEdit->insertPlainText("\n");

  // fixed bug
  int maxSize =
      sizes.empty() ? 0 : *std::max_element(sizes.begin(), sizes.end());
  int minSize =
      sizes.empty() ? 0 : *std::min_element(sizes.begin(), sizes.end());
  int avgSize = sizes.empty() ? 0 : std::reduce(sizes.begin(), sizes.end()) / n;

  textEdit->insertPlainText("Max size:\t");
  textEdit->insertPlainText(QString::number(maxSize));
  textEdit->insertPlainText("\n");
  textEdit->insertPlainText("Min size:\t");
  textEdit->insertPlainText(QString::number(minSize));
  textEdit->insertPlainText("\n");
  textEdit->insertPlainText("Average size:    ");
  textEdit->insertPlainText(QString::number(avgSize));

  // adding menu for saving file
  QMenuBar *menuBar = new QMenuBar();
  QMenu *fileMenu = menuBar->addMenu("File");

  QAction *saveAction = new QAction("Save as image", menuBar);
  fileMenu->addAction(saveAction);

  connect(saveAction, &QAction::triggered, this,
          [this, textEdit]() { saveAsPNG(textEdit); });

  textLayout->setMenuBar(menuBar);
  textLayout->addWidget(textEdit);

  widget->setFixedWidth(700);
  widget->setWindowTitle("Basic information");
  widget->show();
}

void
    TextStatistics::srcDestCount(std::vector<ParsedPacket> parsedPacket) {
  QWidget *widget = new QWidget();
  QVBoxLayout *textLayout = new QVBoxLayout(widget);

  QTextEdit *textEdit = new QTextEdit(widget);
  textEdit->setReadOnly(true);

  std::map<std::pair<std::string, std::string>, int> srcDestMap =
      TextStatistics::getSrcDestMap(parsedPacket);

  textEdit->insertPlainText(
      "Number of packets for each source-destnation pair\n");
  for (auto & it : srcDestMap) {
    std::pair<std::string, std::string> srcDestPair = it.first;
    textEdit->insertPlainText(QString::fromStdString(srcDestPair.first));
    textEdit->insertPlainText(" -> ");
    textEdit->insertPlainText(QString::fromStdString(srcDestPair.second));
    textEdit->insertPlainText(":  ");
    textEdit->insertPlainText(QString::number(it.second));
    textEdit->insertPlainText("\n");
  }

  textEdit->setContextMenuPolicy(Qt::DefaultContextMenu);

  QMenuBar *menuBar = new QMenuBar();
  QMenu *fileMenu = menuBar->addMenu("File");

  QAction *saveAction = new QAction("Save as image", menuBar);
  fileMenu->addAction(saveAction);

  connect(saveAction, &QAction::triggered, this,
          [this, textEdit]() { saveAsPNG(textEdit); });

  textLayout->addWidget(menuBar);

  textLayout->addWidget(textEdit);

  widget->setFixedWidth(700);
  widget->setWindowTitle("Source to destination count");
  widget->show();
}

void
    TextStatistics::saveAsPNG(QTextEdit *textEdit) {
  QPixmap pixmap = textEdit->grab();
  QImage image = pixmap.toImage();

  QString filePath =
      QFileDialog::getSaveFileName(this, "Save Image", "", "PNG Image (*.png)");

  if (!filePath.isEmpty()) {
    image.save(filePath);
  }
}

void
    TextStatistics::infoAboutSession(
        std::vector<std::pair<uint64_t, uint64_t>> packetCount,
        std::vector<std::pair<long, long>> captureTimes) {
  QWidget *widget = new QWidget();
  QVBoxLayout *textLayout = new QVBoxLayout(widget);

  QTextEdit *textEdit = new QTextEdit(widget);
  textEdit->setReadOnly(true);

  for (auto &v : captureTimes) {
    long startTime = v.first;
    long endTime = v.second;
    long duration = v.second - v.first;

    textEdit->insertPlainText("Start time of session since epoch:   ");
    textEdit->insertPlainText(QString::number(startTime) + "s");
    textEdit->insertPlainText("\n");

    textEdit->insertPlainText("End time of session since epoch:    ");
    textEdit->insertPlainText(QString::number(endTime) + "s");
    textEdit->insertPlainText("\n");

    textEdit->insertPlainText("Start time of session:   ");
    textEdit->insertPlainText(ctime(&(startTime)));
    textEdit->insertPlainText("End time of session:   ");
    textEdit->insertPlainText(ctime(&(endTime)));
    textEdit->insertPlainText("\n");

    textEdit->insertPlainText("Duration:   ");
    textEdit->insertPlainText(QString::number(duration) + "s");
    textEdit->insertPlainText("\n");
    textEdit->insertPlainText("\n");
  }

  for (auto &x : packetCount) {
    uint64_t packetsReceived = x.first;
    uint64_t packetsDropped = x.second;

    textEdit->insertPlainText("Number of received packets:   ");
    textEdit->insertPlainText(QString::number(packetsReceived));
    textEdit->insertPlainText("\n");

    textEdit->insertPlainText("Number of dropped packets:   ");
    textEdit->insertPlainText(QString::number(packetsDropped));
    textEdit->insertPlainText("\n");
  }

  QMenuBar *menuBar = new QMenuBar();
  QMenu *fileMenu = menuBar->addMenu("File");

  QAction *saveAction = new QAction("Save as image", menuBar);
  fileMenu->addAction(saveAction);

  connect(saveAction, &QAction::triggered, this,
          [this, textEdit]() { saveAsPNG(textEdit); });

  textLayout->setMenuBar(menuBar);

  textLayout->addWidget(textEdit);
  widget->setFixedWidth(700);
  widget->setFixedWidth(400);
  widget->setWindowTitle("Basic session information");

  widget->show();
}
