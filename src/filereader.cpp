#include "filereader.h"

FileReader::FileReader(std::string &path,
                       std::shared_ptr<SessionData> sessionData)
    : m_path(path)
    , m_sessionData(sessionData) {

  m_reader = pcpp::IFileReaderDevice::getReader(path);

  if (!m_reader->open()) {
    throw std::ios_base::failure("Failed to open the file");
  }
}

FileReader::~FileReader() {
  m_reader->close();
  m_sessionData.reset();
  delete m_reader;
}

void
    FileReader::readPcapFile() {
  pcpp::RawPacket rawPacket;
  // a while loop that will continue as long as there are packets in the input
  // file matching the BPF filter
  while (m_reader->getNextPacket(rawPacket)) {
    m_sessionData->push(&rawPacket);
  }
}

std::string
    FileReader::getFilePath() {
  return m_path;
}

void
    FileReader::set_filter(std::vector<pcpp::GeneralFilter *> &filter) {
  if (!filter.empty()) {
    pcpp::AndFilter andFilter(filter);

    if (!m_reader->setFilter(andFilter)){
      throw std::invalid_argument(
          "can't parse filter! expression rejects all filters");
    }
  }
}
