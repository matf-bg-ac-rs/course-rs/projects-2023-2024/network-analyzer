#!/bin/bash

set -e

# Set default path to source directory
BASEDIR=$(dirname "$0")
# Set default build type to "Release" if not provided as a parameter
BUILD_TYPE="Release"

# Parse command-line options
while getopts ":b:d:" opt; do
  case $opt in
    b)
      BUILD_TYPE="$OPTARG"
      ;;
    d)
      BASEDIR="$OPTARG"
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# After all arguments are parsed, we do the actual work

pushd "$BASEDIR"

rm -rf build

conan install . --output-folder=build --build=missing --settings=build_type="$BUILD_TYPE"
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake -DCMAKE_BUILD_TYPE="$BUILD_TYPE" # don't forget to set cmake prefix path if the build fails /home/usernam/Qt/6.7.2/gcc_64/lib/cmake/Qt6 
cmake --build .

