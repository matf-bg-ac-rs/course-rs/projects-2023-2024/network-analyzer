#ifndef PROTOCOLS_H
#define PROTOCOLS_H

#include <map>
#include <string>

#include "pcapplusplus/ProtocolType.h"
// all recognizable protocols                             (there will be more
// maybe?)
#include "pcapplusplus/ArpLayer.h"
#include "pcapplusplus/DnsLayer.h"
#include "pcapplusplus/EthLayer.h"
#include "pcapplusplus/HttpLayer.h"
#include "pcapplusplus/IPv4Layer.h"
#include "pcapplusplus/IPv6Layer.h"
#include "pcapplusplus/TcpLayer.h"
#include "pcapplusplus/UdpLayer.h"
#include "pcapplusplus/VlanLayer.h"
#include "qcolor.h"
#include <QDebug>

class Protocols {
  public:
    const static QColor
        getColor(pcpp::ProtocolType protocol);
    const static std::string
        toString(pcpp::ProtocolType protocol);
    const static std::map<pcpp::ProtocolType, std::string> &
        getProtocolToString();
    const static std::map<std::string, pcpp::ProtocolType> &
        getStringToProtocol();
    const static std::vector<pcpp::ProtocolType> &
        getSupportedFilterProtocol();

  private:
    const static std::map<pcpp::ProtocolType, std::string> m_protocolToString;
    const static std::map<std::string, pcpp::ProtocolType> m_stringToProtocol;
    const static std::map<pcpp::ProtocolType, QColor> m_coloringRules;
    // encodes protocols that aren't supported in filters
    //(man pcap-filter)
    const static std::vector<pcpp::ProtocolType> m_supportedFilterProtocols;
    // maybe i have to check ?
    const static std::string m_unsupported;

    template <typename K, typename V>
    static std::map<V, K>
        reverse_map(const std::map<K, V> &m) {
      std::map<V, K> r;
      for (const auto &kv : m)
        r[kv.second] = kv.first;
      return r;
    }
};

#endif // PROTOCOLS_H
