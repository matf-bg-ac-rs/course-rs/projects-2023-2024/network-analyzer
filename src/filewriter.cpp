#include "filewriter.h"

FileWriter::FileWriter(std::string &path,
                       const pcpp::LinkLayerType linkLayerType)
    : m_linkLayerType(linkLayerType) {
  m_pcapWriter = new pcpp::PcapFileWriterDevice(path, linkLayerType);

  if (!m_pcapWriter->open()) {
    throw std::invalid_argument("Cannot open *.pcap file for writing.");
  }
}

FileWriter::~FileWriter() {
  m_pcapWriter->close();
  delete m_pcapWriter;
}

void FileWriter::writeToFile(pcpp::RawPacket *packetToWrite) {
  m_pcapWriter->writePacket(*packetToWrite);
}
