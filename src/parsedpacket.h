#ifndef PARSEDPACKET_H
#define PARSEDPACKET_H

#include "protocols.h"
#include <QColor>
#include <QDebug>
#include <iomanip>
#include <pcapplusplus/Packet.h>
#include <sstream>
#include <string>
#include <vector>

//(const and &?)
class ParsedPacket {
  public:
    ParsedPacket(pcpp::Packet &packet);
    int
        getSize() const;

    std::string &
        getSrcAdress();
    std::string &
        getDestAdress();
    std::pair<uint16_t, uint16_t>
        getPortSrcAndDest();

    std::vector<pcpp::ProtocolType> &
        getProtocols();
    std::string
        outputForGUI();
    std::string &
        getPacketBytes() ;
    std::string &
        getPacketBytesAscii() ;
    std::string
        protocolsToString();

    std::vector<std::string> &
        getLayerInfo();

    std::vector<int> &
        getDataLen();
    std::vector<int> &
        getPayload();
    std::vector<int> &
        getHeaderSize();
    QColor &
        getPacketCol();
    bool
        getStateBadTcp() const;

  private:
    int m_size;
    //(srcIp, dstIp)
    std::pair<std::string, std::string> m_ipAddr;
    std::vector<std::string> m_layerInfo;
    QColor m_packetCol;

    void
        initializeLayerInfo(pcpp::Packet &packet);
    std::vector<pcpp::ProtocolType> m_protocol;
    std::string m_bytesToAscii;
    std::string m_rawBytes;
    std::string
        getPacketBytes(pcpp::RawPacket *rawPacket) const;
    std::string
        getPacketBytesAscii(pcpp::RawPacket *rawPacket) const;
    std::pair<uint16_t, uint16_t> m_srcDestPorts;

    std::pair<uint16_t, uint16_t>
        parsePortDestSrc(const pcpp::Packet &packet);
    std::pair<std::string, std::string>
        parseSrcAndDest(const pcpp::Packet &packet);
    // std::string parseHTTP(const pcpp::Packet& packet);

    // DON'T TOUCH THIS FUNC
    template <typename T>
    std::string
        parseLayer(const pcpp::Packet &packet,
                   pcpp::ProtocolType protocolType) {
      std::string result = "";

      if (packet.isPacketOfType(protocolType)) {
        T *layer = packet.getLayerOfType<T>();
        return layer->toString() +
               "\nHeader length: " + std::to_string(layer->getHeaderLen()) +
               "[B], Layer payload: " +
               std::to_string(layer->getLayerPayloadSize()) +
               "[B], Total data: " + std::to_string(layer->getDataLen()) +
               "[B]\n";
      }
      return result;
    }
};
#endif // PARSEDPACKET_H
