#ifndef TEXTSTATISTICS_H
#define TEXTSTATISTICS_H

#include "parsedpacket.h"
#include <QMainWindow>
#include <QMenuBar>
#include <QObject>
#include <QTextEdit>
#include <QWidget>
class TextStatistics : public QWidget {
    Q_OBJECT
  public:
    explicit TextStatistics(QWidget *parent = nullptr);
    void
        size(std::vector<ParsedPacket> parsedPacket);
    double
        numElementsInRange(int a, int b, QVector<int> &v);
    void
        srcDestCount(std::vector<ParsedPacket> parsedPacket);
    std::map<std::pair<std::string, std::string>, int>
        getSrcDestMap(std::vector<ParsedPacket> parsedPacket);
    void
        infoAboutSession(std::vector<std::pair<uint64_t, uint64_t>> packetCount,
                         std::vector<std::pair<long, long>> captureTimes);
  public slots:
    void
        saveAsPNG(QTextEdit *textEdit);
};

#endif // TEXTSTATISTICS_H
