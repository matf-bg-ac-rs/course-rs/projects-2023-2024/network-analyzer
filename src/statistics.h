#ifndef STATISTICS_H
#define STATISTICS_H

#include "sessiondata.h"
#include <QMainWindow>
#include <QtCharts/QAreaSeries>
#include <QtCharts/QBarSet>
#include <QtWidgets>
#include <vector>

class Statistics {
  public:
    Statistics(std::vector<ParsedPacket> &parsed_packets);
    QVector<int>
        count_protocols(QVector<QString> &names);
    QVector<int>
        count_sizes(QVector<QString> &names,
                    std::vector<std::pair<int, int>> &intervals);

  public slots:
    void
        bar_chart( std::string option);
    void
        pie_chart(std::string option);

  private:
    std::vector<ParsedPacket> m_parsed_packets;
};

#endif // STATISTICS_H
