#include "protocols.h"

const std::string Protocols::m_unsupported = "Unknown protocol";

const std::map<pcpp::ProtocolType, std::string> Protocols::m_protocolToString =
    {{pcpp::Ethernet, "Eth"},
     {pcpp::EthernetDot3, "EthDot3Layer"},
     {pcpp::MPLS, "MPLS"},
     {pcpp::VLAN, "VLAN"},
     {pcpp::IPv4, "IPv4"},
     {pcpp::IPv6, "IPv6"},
     {pcpp::ARP, "ARP"},
     {pcpp::TCP, "TCP"},
     {pcpp::SSL, "SSL"},
     {pcpp::UDP, "UDP"},
     {pcpp::ICMP, "ICMP"},
     {pcpp::DNS, "DNS"},
     {pcpp::FTP, "FTP"},
     {pcpp::SSH, "SSH"},
     {pcpp::ICMPv6, "ICMPv6"},
     {pcpp::Telnet, "Telnet"},
     {pcpp::PPPoE, "PPoe"},
     {pcpp::GRE, "GRE"},
     {pcpp::PPP_PPTP, "PPP_PPTP"},
     {pcpp::SSL, "SSL"},
     // https://wiki.wireshark.org/SLL.md
     {pcpp::SLL, "SLL"},
     {pcpp::DHCP, "DHCP"},
     {pcpp::NULL_LOOPBACK, "NULL LOOPBACK"},
     {pcpp::IGMP, "IGMP"},
     {pcpp::IGMPv1, "IGMPv1"},
     {pcpp::IGMPv2, "IGMPv2"},
     {pcpp::IGMPv3, "IGMPv3"},
     {pcpp::GenericPayload, "Generic payload"},
     {pcpp::VXLAN, "VXLAN"},
     {pcpp::SIP, "SIP"},
     {pcpp::SDP, "SDP"},
     {pcpp::PacketTrailer, "Packet trailer"},
     {pcpp::Radius, "Radius"},
     {pcpp::GTP, "GTP"},
     {pcpp::BGP, "BGP"}};
const std::map<pcpp::ProtocolType, QColor> Protocols::m_coloringRules = {
    {pcpp::TCP, QColor(194, 134, 219, 255)},
    {pcpp::UDP, QColor(120, 118, 181, 255)},
    {pcpp::ARP, QColor(209, 78, 212, 255)},
    {pcpp::ICMP, QColor(212, 187, 129, 255)},
    {pcpp::HTTP, QColor(137, 217, 119, 255)},
    {pcpp::IPv6, QColor(45, 40, 84, 255)},
};
const std::map<std::string, pcpp::ProtocolType> Protocols::m_stringToProtocol =
    Protocols::reverse_map(Protocols::m_protocolToString);

const std::vector<pcpp::ProtocolType> Protocols::m_supportedFilterProtocols = {
    pcpp::IPv4, pcpp::IPv6, pcpp::ARP, pcpp::TCP, pcpp::UDP};

const std::string
    Protocols::toString(pcpp::ProtocolType protocol) {
  auto it = Protocols::m_protocolToString.find(protocol);
  return it == Protocols::m_protocolToString.end() ? Protocols::m_unsupported
                                                   : it->second;
}

const std::map<pcpp::ProtocolType, std::string> &
    Protocols::getProtocolToString() {
  return Protocols::m_protocolToString;
}

const std::map<std::string, pcpp::ProtocolType> &
    Protocols::getStringToProtocol() {
  return Protocols::m_stringToProtocol;
}

const std::vector<pcpp::ProtocolType> &
    Protocols::getSupportedFilterProtocol() {
  return Protocols::m_supportedFilterProtocols;
}

const QColor
    Protocols::getColor(pcpp::ProtocolType protocol) {
  auto it = Protocols::m_coloringRules.find(protocol);
  return it == Protocols::m_coloringRules.cend() ? Qt::transparent : it->second;
}
