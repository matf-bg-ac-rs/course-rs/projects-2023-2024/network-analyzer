#include "statistics.h"
#include "parsedpacket.h"
#include "protocols.h"
#include <QDebug>
#include <QVector>
#include <QtAlgorithms>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QCategoryAxis>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QHorizontalStackedBarSeries>
#include <QtCharts/QLegend>
#include <QtCharts/QLineSeries>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>
#include <QtWidgets/QMainWindow>
#include <algorithm>
#include <numeric>
#include <string>
#include <utility>

Statistics::Statistics(std::vector<ParsedPacket> &parsed_packets)
    : m_parsed_packets(parsed_packets) {}

QVector<int>
    Statistics::count_protocols(QVector<QString> &names) {
  std::vector<pcpp::ProtocolType> protocols;
  for (auto pp : m_parsed_packets) {
    for (auto p : pp.getProtocols()) {
      protocols.push_back(p);
    }
  }
  int n = names.size();
  QVector<int> occurence(n);
  for (auto p : protocols) {
    for (int i = 0; i < n; i++)
      if (Protocols::toString(p).compare(names[i].toStdString()) == 0)
        occurence[i]++;
  }
  return occurence;
}
QVector<int>
    Statistics::count_sizes(QVector<QString> &names,
                            std::vector<std::pair<int, int>> &intervals) {
  int n = names.size();
  QVector<int> occurence(n);

  for (auto pp : m_parsed_packets)
    for (int i = 0; i < n; i++) {
      if (i == n - 1) {
        occurence[i]++;
        break;
      }
      int a = intervals[i].first;
      int b = intervals[i].second;
      int size = pp.getSize();
      if (size >= a && size < b) {
        occurence[i]++;
        break;
      }
    }
  return occurence;
}

void
    Statistics::bar_chart(std::string option) {
  QVector<QString> names;
  QVector<int> occurence;
  QString *title = new QString();
  if (option.compare("protocol") == 0) {
    title->append("Protocol occurence");
    names = {"IPv4",
             "IPv6",
             "TCP",
             "UDP",
             "HTTP",
             "DNS",
             "FTP",
             "SSH",
             "SSL",
             "ARP",
             "Generic payload",
             "Unknown protocol"};
    occurence = count_protocols(names);
  } else if (option.compare("size") == 0) {
    title->append("Number of packets in size intervals");
    names = {"0-19", "20-39", "40-79", "80-159", "160-319", "320-639", "other"};
    std::vector<std::pair<int, int>> intervals = {
        {0, 19},    {20, 39},   {40, 79}, {80, 159},
        {160, 319}, {320, 639}, {-1, -1}};
    occurence = count_sizes(names, intervals);
  } else {
    throw std::invalid_argument("No such statistic is available");
  }
  int n = occurence.size();
  QVector<QBarSet *> sets(20);
  for (int i = 0; i != n; ++i) {
    sets[i] = new QBarSet(names[i]);
    *sets[i] << occurence[i];
  }

  QBarSeries *series = new QBarSeries;
  for (int i = 0; i != n; ++i) {
    series->append(sets[i]);
  }

  QChart *chart = new QChart();

  chart->addSeries(series);
  chart->setTitle(*static_cast<const QString *>(title));
  chart->setAnimationOptions(QChart::AllAnimations);

  auto axisX = new QBarCategoryAxis;
  chart->addAxis(axisX, Qt::AlignBottom);
  series->attachAxis(axisX);

  auto axisY = new QValueAxis;
  auto upper_limit =
      qMax(*std::max_element(occurence.begin(), occurence.end()), 0);
  axisY->setRange(0, upper_limit);
  chart->addAxis(axisY, Qt::AlignLeft);
  series->attachAxis(axisY);

  chart->legend()->setVisible(true);
  chart->legend()->setAlignment(Qt::AlignBottom);

  QChartView *chartView = new QChartView(chart);
  chartView->setVisible(true);
  chartView->resize(800, 600);
  chartView->setWindowTitle(*static_cast<const QString *>(title));
  chartView->show();

  delete title;

}

void
    Statistics::pie_chart(std::string option) {
  // define slices and percentage for each of them
  QPieSeries *series = new QPieSeries();
  QVector<QString> names;
  QVector<double> percentage;
  QString *title = new QString();
  int n;

  if (option.compare("protocol") == 0) {
    title->append("Protocol occurence");
    names = {"IPv4",
             "IPv6",
             "TCP",
             "UDP",
             "HTTP",
             "DNS",
             "FTP",
             "SSH",
             "SSL",
             "ARP",
             "Generic payload",
             "Unknown protocol"};
    n = names.size();
    QVector<int> occurence = count_protocols(names);
    std::transform(
        occurence.cbegin(), occurence.cend(), std::back_inserter(percentage),
        [n](const auto &occ) { return static_cast<double>(occ) / n; });
  } else if (option.compare("size") == 0) {
    title->append("Number of packets in size intervals");
    names = {"0-19", "20-39", "40-79", "80-159", "160-319", "320-639", "other"};
    std::vector<std::pair<int, int>> intervals = {
        {0, 19},    {20, 39},   {40, 79}, {80, 159},
        {160, 319}, {320, 639}, {-1, -1}};
    n = names.size();
    QVector<int> occurence = count_sizes(names, intervals);
    std::transform(
        occurence.cbegin(), occurence.cend(), std::back_inserter(percentage),
        [n](const auto &occ) { return static_cast<double>(occ) / n; });
  } else {
    throw std::invalid_argument("No such statistic is available");
  }

  n = names.size();
  for (int i = 0; i < n; i++) {
    series->append(names[i], percentage[i]);
  }

  QVector<QPieSlice *> slice(20);
  for (int i = 0; i < n; i++) {
    slice[i] = series->slices().at(i);
    slice[i]->setLabelVisible();
    slice[i]->setLabelFont(QFont("Arial", 8));
  }

  QChart *chart = new QChart();

  chart->addSeries(series);
  chart->setTitle(*static_cast<const QString *>(title));
  chart->legend()->hide();

  QChartView *chartView = new QChartView(chart);
  chartView->setRenderHint(QPainter::Antialiasing);

  chartView->setVisible(true);
  chartView->resize(800, 600);
  chartView->setWindowTitle(*static_cast<const QString *>(title));
  chartView->show();

  delete title;
}
