#ifndef CAPDEVICE_H
#define CAPDEVICE_H

#include "pcapplusplus/PcapFileDevice.h"
#include "pcapplusplus/PcapFilter.h"
#include "pcapplusplus/PcapLiveDeviceList.h"
#include "pcapplusplus/SystemUtils.h"
#include "stdlib.h"
#include <iostream>

#include "sessiondata.h"
#include <exception>
#include <memory>
#include <string>
#include <typeinfo>

class CapDevice {
  public:
    // packetQueue and filter can't be consts cause of pcpp library!
    CapDevice(const std::string &interfaceName = {""},
              std::shared_ptr<SessionData> sessionData = nullptr);
    ~CapDevice();
    void
        start_capture();
    void
        stop_capture();

    // in seconds since start of epoch (Unix systems)
    long
        startCaptureTime() const;
    long
        endCaptureTime() const;
    long
        captureSessionDuration() const;

    void

        set_filter(std::vector<pcpp::GeneralFilter *> &filter);

    pcpp::LinkLayerType
        getLinkLayerType() const;

  private:
    void
        setStartCaptureTime();
    void
        setEndCaptureTime();

    pcpp::PcapLiveDevice *m_dev;
    std::string m_interfaceName;
    std::shared_ptr<SessionData> m_sessionData;
    pcpp::IPcapDevice::PcapStats m_stats;
    long m_start, m_end;
};

#endif // CAPDEVICE_H
