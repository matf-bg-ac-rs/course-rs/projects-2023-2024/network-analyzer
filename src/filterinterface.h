#ifndef FILTERINTERFACE_H
#define FILTERINTERFACE_H

#include "protocols.h"
#include <QDialog>
#include <pcapplusplus/PcapFilter.h>
#include <set>
#include <string>
#include <vector>

namespace Ui {
class FilterInterface;
}

class FilterInterface : public QDialog {
    Q_OBJECT

  public:
    explicit FilterInterface(QWidget *parent = nullptr);
    ~FilterInterface();

    std::vector<pcpp::GeneralFilter *> &
        filters();

  public slots:
    void
        IPSrcCheck_stateChanged(int arg1);

    void
        IPDestCheck_stateChanged(int arg1);

    void
        srcPortCheck_clicked();

    void
        destPortCheck_clicked();

    void
        submitFilters_accepted();

  private slots:
    void
        spinBox_editingFinished();

  private:
    Ui::FilterInterface *ui;
    std::vector<pcpp::GeneralFilter *> m_filters;
    std::string m_srcIp, m_dstIp;
    bool m_portSrc, m_portDst;
    uint64_t m_port;
};

#endif // FILTERINTERFACE_H
