#ifndef FILEWRITER_H
#define FILEWRITER_H

#include "pcapplusplus/PcapFileDevice.h"
#include "vector"
#include <QDebug>
#include <QObject>
#include <QThread>
#include <exception>
#include <iostream>
#include <string>

class FileWriter : public QObject {
    Q_OBJECT

  public:
    FileWriter(std::string &path, const pcpp::LinkLayerType linkLayerType);
    ~FileWriter();

  public slots:
    void
        writeToFile(pcpp::RawPacket *packet);

  private:
    const std::string m_path;
    pcpp::PcapFileWriterDevice *m_pcapWriter;
    pcpp::LinkLayerType m_linkLayerType;
};

#endif // FILEWRITER_H
