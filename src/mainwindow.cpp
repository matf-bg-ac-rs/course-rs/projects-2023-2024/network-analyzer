#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "TextStatistics.h"
#include "capdevice.h"
#include "filereader.h"
#include "filewriter.h"
#include "filterinterface.h"
#include "parsedpacket.h"
#include "pcapplusplus/PcapLiveDeviceList.h"
#include "protocols.h"
#include "qevent.h"
#include "sessiondata.h"
#include "statistics.h"
#include <QClipboard>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QString>
#include <iostream>
#include <string>
#include <vector>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {

  ui->setupUi(this);

  displayAllDevices();
  m_filterInterface = std::make_unique<FilterInterface>();
  m_sessionData = std::make_shared<SessionData>();

  // labels and find line edit + raw and ascii data text edits
  ui->findItems->hide();
  ui->capturedLbl->hide();
  ui->rawAsciiLbl->hide();
  ui->listWidgetPackets->hide();
  ui->rawDataTxt->hide();
  ui->rawDataAscii->hide();

  // clear stop
  ui->clearBtn->setEnabled(false);
  ui->stopCaptureBtn->setEnabled(false);

  // file and find packet
  ui->actionFind_Packet->setEnabled(false);
  ui->actionReset_found->setEnabled(false);
  ui->actionSave->setEnabled(false);
  ui->actionSave_as->setEnabled(false);

  // text stats
  ui->actionBasic_session_information->setEnabled(false);
  ui->actionBasic_text_information->setEnabled(false);
  ui->actionSource_to_destination_count->setEnabled(false);

  // graph stats
  ui->menuPie_Chart->setEnabled(false);
  ui->menuBar_Chart->setEnabled(false);



}

MainWindow::~MainWindow() {
  m_filterInterface.reset();
  m_capDevice.reset();
  //  destruct(m_sessionData);
  delete ui;
}

void
    MainWindow::showErrorMessage(const std::string &errorMsg) {
  QMessageBox *exceptionBox = new QMessageBox();
  exceptionBox->setText(QString::fromStdString(errorMsg));
  exceptionBox->show();
}

void
    MainWindow::on_startCaptureBtn_clicked() {
  ui->startCaptureBtn->setEnabled(false);
  ui->clearBtn->setDisabled(true);

  ui->stopCaptureBtn->setEnabled(true);
  ui->actionFind_Packet->setEnabled(true);
  ui->actionSave_as->setEnabled(true);
  // text stats
  ui->actionBasic_text_information->setEnabled(true);
  ui->actionSource_to_destination_count->setEnabled(true);

  // ne sme clear nakon capa ima da baci segfault jer se memorija onda ne
  // oslobadja ispravno
  QString interfaceActive = ui->listWidgetInterfaces->currentItem()->text();

  std::string conv = interfaceActive.toStdString();

  QObject::connect(m_sessionData.get(), &SessionData::packetPushed, this,
                   &MainWindow::on_packet_arrived);

  std::vector<pcpp::GeneralFilter *> filters = m_filterInterface->filters();
  try {
    m_capDevice = std::make_unique<CapDevice>(conv, m_sessionData);
  } catch (std::invalid_argument &e) {

    showErrorMessage(e.what());
    m_capDevice.reset();
  }
  try {
    m_capDevice->set_filter(filters);

  } catch (std::invalid_argument &e) {

    showErrorMessage(e.what());
  }

  ui->listWidgetInterfaces->setDisabled(true);
  ui->actionSave->setDisabled(false);
  ui->actionSave_as->setDisabled(false);

  ui->capturedLbl->show();
  ui->listWidgetPackets->show();
  ui->rawAsciiLbl->show();
  ui->rawDataTxt->show();
  ui->rawDataAscii->show();

  QMessageBox *msgSaveBox = new QMessageBox();

  msgSaveBox->setText("Save packets to a file?");
  msgSaveBox->escapeButton();

  QPushButton *yesButton =
      msgSaveBox->addButton(tr("Yes"), QMessageBox::ActionRole);
  msgSaveBox->addButton(QMessageBox::Abort);
  msgSaveBox->exec();

  if (msgSaveBox->clickedButton() == yesButton) {
    std::string file =
        QFileDialog::getSaveFileName(this, "Save file", QDir::currentPath())
            .toStdString();

    try {
      m_fileWriter =
          std::make_unique<FileWriter>(file, m_capDevice->getLinkLayerType());
      QObject::connect(m_sessionData.get(), &SessionData::writePacketToFile,
                       m_fileWriter.get(), &FileWriter::writeToFile);
    } catch (std::invalid_argument &e) {
      m_fileWriter.reset();
      showErrorMessage(e.what());
    }
  }

  m_capDevice->start_capture();
}

void
    MainWindow::on_packet_arrived(ParsedPacket parsedPacket) {

  ui->rawDataTxt->isReadOnly();
  ui->rawDataAscii->isReadOnly();
  ui->listWidgetPackets->addItem(
      QString::fromStdString(parsedPacket.outputForGUI()));
  QColor col = parsedPacket.getPacketCol();
  int lastRow = ui->listWidgetPackets->count() - 1;
  ui->listWidgetPackets->item(lastRow)->setBackground(col);
}

// OPEN FILE
void
    MainWindow::on_actionOpen_triggered() {
  QString filter_only_pcap_ext_files = "PCap files (*.pcap)";
  std::string filepath =
      (QFileDialog::getOpenFileName(this, "Open file", QDir::currentPath(),
                                    filter_only_pcap_ext_files))
          .toStdString();

  std::vector<pcpp::GeneralFilter *> filters = m_filterInterface->filters();

  try {
    FileReader reader(filepath, m_sessionData);
    reader.set_filter(filters);

    ui->startCaptureBtn->setDisabled(true);
    ui->stopCaptureBtn->setDisabled(true);
    ui->clearBtn->setDisabled(false);
    // text stats
    ui->actionBasic_text_information->setEnabled(true);
    ui->actionSource_to_destination_count->setEnabled(true);

    // graph stats
    ui->menuPie_Chart->setEnabled(true);
    ui->menuBar_Chart->setEnabled(true);
    ui->actionFind_Packet->setEnabled(true);

    ui->capturedLbl->show();
    ui->rawAsciiLbl->show();
    ui->rawDataTxt->show();
    ui->rawDataAscii->show();

    ui->listWidgetPackets->show();

    QObject::connect(m_sessionData.get(), &SessionData::packetPushed, this,
                     &MainWindow::on_packet_arrived);

    reader.readPcapFile();

  } catch (std::ios_base::failure &e) {
    showErrorMessage(e.what());
    return;
  } catch (std::invalid_argument &e) {
    showErrorMessage(e.what());
    return;
  }

  std::vector<ParsedPacket> parsed_packets = m_sessionData->getParsedPackets();

  m_statistics = std::make_unique<Statistics>(parsed_packets);
  m_textStatistics = std::make_unique<TextStatistics>();
}

void
    MainWindow::on_actionQuit_triggered() {
  on_clearBtn_clicked();
  QApplication::quit();
}

void
    MainWindow::on_stopCaptureBtn_clicked() {
  ui->clearBtn->setDisabled(false);
  ui->actionOpen->setDisabled(true);
  ui->actionBasic_session_information->setDisabled(false);
  ui->menuPie_Chart->setEnabled(true);
  ui->menuBar_Chart->setEnabled(true);

  ui->stopCaptureBtn->setDisabled(true);
  m_capDevice->stop_capture();

  m_capDevice.reset();
  m_fileWriter.reset();

  std::vector<ParsedPacket> parsed_packets = m_sessionData->getParsedPackets();

  m_statistics = std::make_unique<Statistics>(parsed_packets);
  m_textStatistics = std::make_unique<TextStatistics>();
}

void
    MainWindow::on_listWidgetPackets_currentRowChanged(int currentRow) {
  ui->rawDataTxt->clear();
  ui->rawDataAscii->clear();

  if (currentRow != -1) {
    ui->rawDataTxt->append(QString::fromStdString(
        m_sessionData->getParsedPackets()[currentRow].getPacketBytes()));
    ui->rawDataAscii->append(QString::fromStdString(
        m_sessionData->getParsedPackets()[currentRow].getPacketBytesAscii()));
    ui->rawDataAscii->isReadOnly();
    ui->rawDataTxt->isReadOnly();
  }
}

void
    MainWindow::on_clearBtn_clicked() {
  // allow for capturing and opening existing files again

  ui->listWidgetInterfaces->setDisabled(false);
  ui->startCaptureBtn->setDisabled(false);
  ui->actionOpen->setDisabled(false);

  ui->actionFind_Packet->setEnabled(false);
  // text stats
  ui->actionBasic_session_information->setDisabled(true);
  ui->actionBasic_text_information->setEnabled(false);
  ui->actionSource_to_destination_count->setEnabled(false);

  // graph stats
  ui->menuPie_Chart->setEnabled(false);
  ui->menuBar_Chart->setEnabled(false);

  // 6 ako se nisam zabrojala
  QMessageBox *msgBox = new QMessageBox();

  msgBox->setText("Delete all packets captured?");
  msgBox->escapeButton();

  QPushButton *yesButton =
      msgBox->addButton(tr("Yes"), QMessageBox::ActionRole);
      msgBox->addButton(QMessageBox::Abort);
  msgBox->exec();

  if (msgBox->clickedButton() == yesButton) {
    //  destruct(m_sessionData);
    m_statistics.reset();
    m_textStatistics.reset();
    m_filterInterface.reset(new FilterInterface());
    // sto bih unistavala pa kreirala novo <- pa uklone se svi podaci iz
    // vektora i iz pocetka se puni taj vektor

    m_sessionData.reset();
    m_sessionData.reset(new SessionData);

    // ovo moze samo preparefornextSession da bude?  dodala sam ga ovde jer
    // nema mi smisla da se sklanja ako se ne brise
    ui->listWidgetPackets->clear();
    ui->capturedLbl->hide();
    ui->listWidgetPackets->hide();
    ui->rawAsciiLbl->hide();
    ui->rawDataTxt->hide();
    ui->rawDataAscii->hide();
    ui->findItems->hide();
  }
}

void
    MainWindow::on_actionCopy_triggered() {
  if (ui->listWidgetPackets->currentRow() != -1) {
    QClipboard *clipboard = QGuiApplication::clipboard();
    clipboard->setText(ui->listWidgetPackets->currentItem()->text());
  }
}
void
    MainWindow::on_actionNext_triggered() {
  if (ui->listWidgetPackets->currentRow() != -1) {
    ui->listWidgetPackets->setCurrentRow(ui->listWidgetPackets->currentRow() +
                                         1);
  }
}

void
    MainWindow::on_actionPrevious_triggered() {
  if (ui->listWidgetPackets->currentRow() != -1) {
    int newRow = ui->listWidgetPackets->currentRow() - 1;
    if (newRow != -1)
      ui->listWidgetPackets->setCurrentRow(newRow);
  }
}
bool
    MainWindow::on_actionFind_Packet_triggered() {
  ui->findItems->setPlaceholderText("...insert protocol or ip addr here");
  ui->findItems->show();
  ui->actionReset_found->setDisabled(false);
  return true;
}

QList<QListWidgetItem *>
    MainWindow::on_findItems_editingFinished() {
  QList<QListWidgetItem *> list = ui->listWidgetPackets->findItems(
      ui->findItems->text(), Qt::MatchContains);
  for (QListWidgetItem *item : list) {
    item->setBackground(Qt::green);
    item->setForeground(Qt::black);
  }
  return list;
}
void
    MainWindow::on_actionReset_found_triggered() {
  int lastRow = ui->listWidgetPackets->count() - 1;

  for (int i = 0; i < lastRow; ++i) {
    QColor col = m_sessionData->getParsedPackets()[i].getPacketCol();
    ui->listWidgetPackets->item(i)->setBackground(col);
    ui->listWidgetPackets->item(i)->setForeground(Qt::white);
  }
  ui->findItems->clear();
  ui->findItems->hide();
  ui->actionReset_found->setDisabled(true);
}
void
    MainWindow::on_actionCapture_filters_2_triggered() {

  m_filterInterface->setModal(true);
  m_filterInterface->exec();
}

void
    MainWindow::on_actionBasic_text_information_triggered() {
  m_textStatistics->size(m_sessionData->getParsedPackets());
}

void
    MainWindow::on_actionBasic_session_information_triggered() {
  m_textStatistics->infoAboutSession(m_sessionData->getPacketCount(),
                                     m_sessionData->getCaptureTimes());
}

void
    MainWindow::on_actionSource_to_destination_count_triggered() {
  m_textStatistics->srcDestCount(m_sessionData->getParsedPackets());
}

void
    MainWindow::on_pie_protocol_triggered() {
  m_statistics->pie_chart("protocol");
}

void
    MainWindow::on_pie_size_triggered() {
  m_statistics->pie_chart("size");
}

void MainWindow::on_bar_protocol_triggered() {
  m_statistics->bar_chart("protocol");
}

void
    MainWindow::on_bar_size_triggered() {
  m_statistics->bar_chart("size");
}

void
    MainWindow::displayAllDevices() {
  const std::vector<pcpp::PcapLiveDevice *> &devList =
      pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDevicesList();
  for (auto dev : devList) {
    QString interfaceName = QString::fromStdString(dev->getName());
    if (interfaceName != "nflog" && interfaceName != "nfqueue")
      ui->listWidgetInterfaces->addItem(interfaceName);
    ui->listWidgetInterfaces->count() - 1;
  }
  if (ui->listWidgetInterfaces->count() == 0) {
    QMessageBox *noInterfaces = new QMessageBox();
    noInterfaces->setText("No interfaces available");
    noInterfaces->addButton(tr("Ok"), QMessageBox::ActionRole);
    noInterfaces->setModal(true);
    noInterfaces->show();
  }
  ui->listWidgetInterfaces->setCurrentRow(0);
}

void
    MainWindow::on_listWidgetInterfaces_itemDoubleClicked(QListWidgetItem *item) {

  const std::vector<pcpp::PcapLiveDevice *> &devList =
      pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDevicesList();
  for (auto dev : devList) {
    if (item->text() == QString::fromStdString(dev->getName())) {
      QMessageBox *qbox = new QMessageBox();
      bool dns = dev->getDnsServers().size() > 0 ? true : false;

      QString textInfo =
          "Interface info: \nInterface name: " +
          QString::fromStdString(dev->getName()) +
          "\nInterface description: " + QString::fromStdString(dev->getDesc()) +
          "\nMAC address: " +
          QString::fromStdString((dev->getMacAddress()).toString()) +
          "\nDefault gateway: " +
          QString::fromStdString((dev->getDefaultGateway()).toString()) +
          "\nInterface MTU: " +
          QString::fromStdString(std::to_string(dev->getMtu()));

      if (dns)
        textInfo.append(
            "\nDNS server: " +
            QString::fromStdString(dev->getDnsServers().at(0).toString()));

      qbox->setText(textInfo);
      qbox->show();
      break;
    }
  }
}

void
    MainWindow::extracted(std::string &detailedInfoTxt,
                          std::vector<std::string> &layerInfo) {
  for (const auto &info : layerInfo) {
    detailedInfoTxt.append(info);
  }
}
void
    MainWindow::on_listWidgetPackets_itemDoubleClicked(QListWidgetItem *item) {
  int currentRow = item->listWidget()->currentRow();
  QMessageBox *additionalInfo = new QMessageBox();

  std::string detailedInfoTxt = "Additional information: \n";
  std::vector<std::string> layerInfo =
      (m_sessionData->getParsedPackets()[currentRow]).getLayerInfo();
  extracted(detailedInfoTxt, layerInfo);
  additionalInfo->setText(QString::fromStdString(detailedInfoTxt));
  additionalInfo->show();


  additionalInfo->deleteLater();
}
void MainWindow::readFromFile(const QString &path, TypeOfFile &type) {
  QFile file(path);
  QString aboutTxt;
  QWidget *widget = new QWidget();
  QVBoxLayout *textLayout = new QVBoxLayout(widget);
  QTextEdit *textEdit = new QTextEdit(widget);
  textEdit->setReadOnly(true);

  file.open(QFile::ReadOnly | QFile::Text);
  QTextStream in(&file);

  QPixmap pic(path);
  QLabel *ql = new QLabel(widget);

  switch (type) {
    case TypeOfFile::About:
      widget->setWindowTitle("About");
      widget->setFixedWidth(700);
      widget->setFixedHeight(250);
      aboutTxt = in.readAll();
      textEdit->insertPlainText(aboutTxt);
      textLayout->addWidget(textEdit);
      break;
    case TypeOfFile::HowTo:
      widget->setWindowTitle("How to use");
      aboutTxt = in.readAll();
      textEdit->insertPlainText(aboutTxt);
      textLayout->addWidget(textEdit);
      widget->setFixedWidth(700);
      widget->setFixedHeight(500);
      break;
    case TypeOfFile::ColoringScheme:
      widget->setWindowTitle("Coloring scheme information");
      ql->setFixedHeight(540);
      ql->setFixedWidth(540);
      ql->setPixmap(pic.scaled(540, 540, Qt::KeepAspectRatio));
      widget->setFixedHeight(540);
      widget->setFixedWidth(540);
      break;
  }

  widget->show();

  // file.close();
  // textLayout->deleteLater();
  // textEdit->deleteLater();
  // ql->close();

}

void
    MainWindow::on_actionAbout_triggered() {
  QString path = "../resources/about.txt";
  TypeOfFile type = TypeOfFile::About;
  readFromFile(path, type);

}

void
    MainWindow::on_actionColor_coding_scheme_triggered() {
  QString path = "../resources/coloringRules.png";
  TypeOfFile type = TypeOfFile::ColoringScheme;
  readFromFile(path, type);
}

void
    MainWindow::on_actionHowToUse_triggered() {
  QString path = "../resources/howto.txt";
  TypeOfFile type = TypeOfFile::HowTo;
  readFromFile(path, type);
}
