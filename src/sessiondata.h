#ifndef SESSIONDATA_H
#define SESSIONDATA_H

#include "parsedpacket.h"
#include "pcapplusplus/PcapFileDevice.h"
#include "pcapplusplus/PcapLiveDeviceList.h"
#include <QObject>
#include <chrono>
#include <iostream>
#include <pcapplusplus/Packet.h>
#include <queue>
#include <vector>

class SessionData : public QObject {
    Q_OBJECT
  public:
    SessionData();
    ~SessionData();
    void
        push(pcpp::RawPacket *rawPacket);

    void
        savePacketCount(uint64_t packetsRevc, uint64_t packetsDropped);
    void
        saveCaptureTime(long start, long end);
    std::vector<ParsedPacket> &
        getParsedPackets();
    // std::vector<pcpp::RawPacket*> getRawPackets() const;

    std::vector<std::pair<long, long>> &
        getCaptureTimes();
    std::vector<std::pair<uint64_t, uint64_t>> &
        getPacketCount();

  signals:
    void
        packetPushed(ParsedPacket parsedPacket);
    void
        writePacketToFile(pcpp::RawPacket *packetToWrite);

  private:
    bool m_writeToFile;
    std::vector<ParsedPacket> m_parsedPacketVector;
    // stores a pair (packetsReceived, packetsDropped) for each packet capture
    // session
    std::vector<std::pair<uint64_t, uint64_t>> m_packetCount;
    // stores a pair of start capture time and end capture time (in num of
    // seconds since epochs)
    std::vector<std::pair<long, long>> m_captureTimes;
    // std::vector<pcpp::RawPacket*> m_rawPacketVector;
};

#endif // SESSIONDATA_H
