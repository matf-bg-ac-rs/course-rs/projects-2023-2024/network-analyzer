#include "capdevice.h"
#include <QDebug>
#include <exception>

// A callback function for the async capture which is called each time a packet
// is captured
static void
    onPacketArrives(pcpp::RawPacket *packet, pcpp::PcapLiveDevice *dev,
                    void *cookie) {
  if (cookie != nullptr) {
    SessionData *sessionData = (SessionData *)cookie;
    sessionData->push(packet);
  }
}

CapDevice::~CapDevice() {
  m_sessionData.reset();
  m_dev->close();
}

CapDevice::CapDevice(const std::string &interfaceName,
                     std::shared_ptr<SessionData> sessionData)
    : m_interfaceName(interfaceName)
    , m_sessionData(sessionData) {
  m_dev = pcpp::PcapLiveDeviceList::getInstance().getPcapLiveDeviceByName(
      m_interfaceName);
  if (m_dev == nullptr)
    throw std::invalid_argument("invalid capture device name");
  if (!m_dev->open())
    throw std::runtime_error("cannot open device");
}

void
    CapDevice::set_filter(std::vector<pcpp::GeneralFilter *> &filter) {
  if (!filter.empty()) {
    std::string result;
    pcpp::AndFilter andFilter(filter);
    andFilter.parseToString(result);

    if

        (!m_dev->setFilter(andFilter))
      throw std::invalid_argument(
          "can't parse filter! expression rejects all filters");
  }
}

void
    CapDevice::setStartCaptureTime() {
  long tmp_ns;
  try {
    pcpp::clockGetTime(m_start, tmp_ns);
  } catch (int e) {
    m_start = -1;
  }
}

void
    CapDevice::setEndCaptureTime() {
  long tmp_ns;
  try {
    pcpp::clockGetTime(m_end, tmp_ns);
  } catch (int e) {
    m_end = -1;
  }
}

long
    CapDevice::startCaptureTime() const {
  return m_start;
}

long
    CapDevice::endCaptureTime() const {
  return m_end;
}

long
    CapDevice::captureSessionDuration() const {
  return m_end - m_start;
}

void
    CapDevice::start_capture() {
  setStartCaptureTime();
  m_dev->startCapture(
      onPacketArrives,
      m_sessionData.get()); // i can't send shared pointer because of called
                            // function prototype
}

void
    CapDevice::stop_capture() {
  m_dev->stopCapture();
  setEndCaptureTime();
  if (m_sessionData) {
    m_sessionData->saveCaptureTime(m_start, m_end);
    m_dev->getStatistics(m_stats);
    m_sessionData->savePacketCount(m_stats.packetsRecv, m_stats.packetsDrop);
  }
}

pcpp::LinkLayerType
    CapDevice::getLinkLayerType() const {
  return m_dev->getLinkType();
}
