#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "TextStatistics.h"
#include "capdevice.h"
#include "filterinterface.h"
#include "parsedpacket.h"
#include "sessiondata.h"
#include "statistics.h"
#include <QMainWindow>
#include <memory>

#include "filewriter.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    // QString getInterfaceName();
    enum TypeOfFile { About, HowTo, ColoringScheme };

    void
        displayAllDevices();
    void
        prepareForNextSession();
    void
        showErrorMessage(const std::string &errorMsg);
    void
        readFromFile(const QString &path, TypeOfFile &);
    void
        extracted(std::string &detailedInfoTxt,std::vector<std::string> &layerInfo);

  public slots:
    void
        on_startCaptureBtn_clicked();

    void
        on_actionOpen_triggered();

    void
        on_actionQuit_triggered();
    void
        on_stopCaptureBtn_clicked();
    void
        on_packet_arrived(ParsedPacket parsedPacket);

  private slots:

    void
        on_clearBtn_clicked();

    void
        on_actionCapture_filters_2_triggered();

    void
        on_actionBasic_text_information_triggered();

    void
        on_actionSource_to_destination_count_triggered();

    void
        on_pie_protocol_triggered();

    void
        on_pie_size_triggered();

    void
        on_bar_protocol_triggered();

    void
        on_bar_size_triggered();

    void
        on_listWidgetPackets_currentRowChanged(int currentRow);

    void
        on_actionCopy_triggered();

    void
        on_actionNext_triggered();

    void
        on_actionPrevious_triggered();

    bool
        on_actionFind_Packet_triggered();

    QList<QListWidgetItem *>
        on_findItems_editingFinished();

    void
        on_actionReset_found_triggered();

    void
        on_actionBasic_session_information_triggered();

    void
        on_listWidgetInterfaces_itemDoubleClicked(QListWidgetItem *item);

    void
        on_listWidgetPackets_itemDoubleClicked(QListWidgetItem *item);

    void
        on_actionAbout_triggered();

    void
        on_actionColor_coding_scheme_triggered();

    void
        on_actionHowToUse_triggered();

  private:
    Ui::MainWindow *ui;
    std::unique_ptr<CapDevice> m_capDevice;
    std::shared_ptr<SessionData> m_sessionData;
    pcpp::RawPacket *packet;

    std::unique_ptr<FilterInterface> m_filterInterface;
    std::unique_ptr<Statistics> m_statistics;
    std::unique_ptr<TextStatistics> m_textStatistics;

    std::unique_ptr<FileWriter> m_fileWriter;
};
#endif // MAINWINDOW_H
