import os, subprocess

print("Select a file to view or type exit to stop the program: ") 

dirlist = os.listdir()
dirlistind = zip(range(len(dirlist)), dirlist) 

for index, files in dirlistind: 
    if(files == "Dockerfile" or files == "pdfreader.py"):
        continue
    print(index + 1, ") ", files, sep="") 
while(True):
    pickpdf = input()

    if(pickpdf == 'exit'):
        break 
    pickpdfint = int(pickpdf)
    filename = dirlist[pickpdfint - 1]

    if(filename[len(filename) - 4 : len(filename)] == '.pdf'): 
        subprocess.call(["evince", dirlist[pickpdfint - 1]])
    else:
        subprocess.call(["ristretto", dirlist[pickpdfint - 1]])
