sudo docker build -t docsreader:v1 .
sudo docker run -it -e DISPLAY -e DBUS_SESSION_BUS_ADDRESS \
    -v /tmp/.X11-unix:/tmp/.X11-unix -v /run:/run \
    --user="$(id --user):$(id --group)" \
    --security-opt apparmor=unconfined \
    -v /dev/sr0:/dev/sr0 \
    -v /dev/cdrom:/dev/cdrom \
    --privileged \
    $1 docsreader:v1
