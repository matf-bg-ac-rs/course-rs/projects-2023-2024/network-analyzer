FROM ubuntu:latest as build

COPY . /network-analyzer

RUN apt update -y

RUN apt install libxcb-xinerama0 -y
RUN apt install build-essential -y
RUN apt install cmake -y
RUN apt install qt6-base-dev -y
RUN apt install libqt6charts6-dev -y
RUN apt install libpcap-dev -y
RUN apt install libxkbcommon-dev -y
RUN apt install libvulkan-dev -y
RUN apt install freeglut3-dev -y
RUN apt install python3 python3-pip -y
RUN pip3 install conan --break-system-packages
RUN conan profile detect

CMD cd /network-analyzer/src && ./build.sh && cd build && ./NetworkAnalyzer

