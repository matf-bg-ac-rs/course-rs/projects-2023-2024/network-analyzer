## Network Analyzer 

Članovi tima:     

[Ana Mihajlović](https://gitlab.com/cholesski)  
[Marijana Čupović](https://gitlab.com/MarijanaCupovic)  
[Simona Jevtović](https://gitlab.com/simona.jevtovic)  
[Anđela Mlađenović](https://gitlab.com/andja11)   
[Katarina Grbović](https://gitlab.com/gkatarina)  


## Šta je Network Analyzer i kako se pokreće?

Network Analyzer je projekat (odnosno program) čiji je zadatak hvatanje i parsiranje paketa na mreži. Pored toga, pruža još mogućnosti i informacija poput statistika i analize paketa i njegovih sirovih podataka. 

Potrebne biblioteke : 
- Qt 6.6.1
- Qt Charts
- PcapPlusPlus (https://pcapplusplus.github.io/docs/install)

Bildovanje pomocu Qt Creatora:
- Klonirati projekat
- Otvoriti src direktorijum
- Importovati buildovanu pcapplusplus biblioteku 
- Kliknuti na build dugme
- Pozicionirati se direktorijum gde se nalazi izvrsni fajl i pokrenuti sa sudo ./NetworkAnalyzer


Demo snimak: https://drive.google.com/file/d/1tX-aD4D9G5NUGf2Wh92ZQUn_M8FJkNph/view?usp=drive_link
